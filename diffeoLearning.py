import numpy as np
from base.testErrors import ClassTestErrors, RegressTestErrors
from base.pointSetMatching import PointSetMatchingParam, PointSetMatching
from base import kernelFunctions as kfun, loggingUtils
from base import pointSets
from base import dataSets
from base import trainTest
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("TKAgg")
#from mnist import MNIST

outputDirPath = '/home/michele/Development/gitDir'
#outputDirPath = '/Users/younes/Development'

plotDim = 3
verbose = False

nlayers = 1
addDim = 1

preDimRed = False

diffeoLearn = 'classif'
diffeoLearn = 'regress'

AllTD = {'helixes3': (100,)}
# AllTD = {'helixes10': (200,)}
# AllTD = {'helixes20': (200,)}
# AllTD = {'RBF': (100,)}
# AllTD = {'Line': (100,)}
AllTD = {'Dolls': (500,)}
# AllTD = {'TwoSegments': (100,)}


def classify(typeData, l1Cost = 1.0, addDim = 1, sigError = 1., randomInit=0.05, removeNullDirs = False,
             dct = False, NTr = 100, NTe = 2000, outputDir = '.'):

    # Set some parameters
    #typeData = 'Dolls'
    localMaps = None
    relearnRate = 1
    u0 = None
    affine = 'none'
    dct = False
    sparseProj = False

    # Create datasets
    x0Tr,x1Tr,x0Te,x1Te,affine,localMaps,addDim = dataSets.dataClassif(typeData, NTr, NTe, affine, localMaps, addDim, dct, sparseProj)

    # if preDimRed:
        # modify datasets with some dimension reduction technique, and set withNormals accordingly in call to pointSetMatching

    # Calculate wTr
    #l1Cost *= np.log10(NTr)
    nclasses = x1Tr.max() + 1
    nTr = np.zeros(nclasses)
    for k in range(nclasses):
        nTr[k] = (x1Tr == k).sum()
    wTr = float(x1Tr.size) / (nTr[x1Tr[:, 0]] * nclasses)[:, np.newaxis]

    # Normalize training and testing data so median value of (|x_k-x_l|^2, k.l = 1,...,NTr) of training set is equal to 1
    dst = np.sqrt(((x0Tr[:, np.newaxis, :] - x0Tr[np.newaxis,:,:])**2).sum(axis=2))
    sigma = np.percentile(dst[np.tril_indices(NTr)], 50)
    print('Estimated sigma:', sigma)
    x0Tr /= sigma
    x0Te /= sigma
    sigma = 1.0

    # Logistic regression training
    fu = pointSets.learnLogisticL2(x0Tr, x1Tr, w=wTr, l1Cost=l1Cost)
    while np.fabs(fu).max() < 1e-8:
        # If fu values too small, re-train with learnLogistic and smaller l1Cost
        l1Cost *= 0.9
        fu = pointSets.learnLogistic(x0Tr, x1Tr, w=wTr, l1Cost=l1Cost)

    # xDef1 = x0Te with column of one's added to left
    xDef1 = np.concatenate((np.ones((x0Te.shape[0], 1)), x0Te), axis=1)

    # Logistic regression classification results on test dataset
    gu = np.argmax(np.dot(xDef1, fu), axis=1)[:, np.newaxis]

    # Remove columns of x0Tr and x0Te corresponding to null vectors (i.e., dimensions) in fu
    if removeNullDirs:
        J = np.nonzero(np.fabs(fu[1:-1, :]).sum(axis=1) > 1e-8)[0]
        x0Tr0 = x0Tr[:,J]
        x0Te0 = x0Te[:,J]
    else:
        x0Tr0 = x0Tr
        x0Te0 = x0Te

    # Create object f from the class PointSetMatching and initialize the attributes of the class
    K1 = kfun.Kernel(name='laplacian', sigma=sigma, order=1)
    sm = PointSetMatchingParam(timeStep=0.1, KparDiff = K1, sigmaError=sigError*np.sqrt(NTr), errorType='classification')
    f = PointSetMatching(trainingSet= (x0Tr0,x1Tr), outputDir=outputDir, param=sm, regWeight=1.,
                         saveTrajectories=True, pplot=True, testSet=(x0Te0, x1Te), addDim = addDim, u0=u0,
                         normalizeInput=False, l1Cost = l1Cost, relearnRate=relearnRate, randomInit=randomInit,
                         affine=affine, testGradient=True, affineWeight=10.,
                         maxIter=1500, plotDim=plotDim, verbose=verbose)

    # Create object testInit from the class ClassTestErrors and initialize the attributes of the class
    testInit = ClassTestErrors()

    # Logistic regression classification error for original test dataset
    testInit.logistic = np.sum(np.not_equal(gu, x1Te) * f.wTe) / f.swTe

    # knn, linSVM, SVM, RF, and mlp training, testing, and error calculations (on original training and test datasets)
    testInit = trainTest.classTrainTest(testInit, f, x0Tr, x1Tr, x0Te, x1Te, 0)

    print(testInit)
    if localMaps is not None:
        K1.localMaps = localMaps

    for k in range(1):
        f.optimizeMatching()

    return f, testInit


def regress(typeData, l1Cost = 1.0, addDim = 1, sigError = 1., randomInit=0.05, removeNullDirs = False,
             dct = False, NTr = 100, NTe = 2000, outputDir = '.'):

    # Set some parameters
    #typeData = 'Dolls'
    localMaps = None
    relearnRate = 1
    u0 = None
    affine = 'none'
    dct = False
    sparseProj = False

    # Create datasets
    x0Tr,x1Tr,x0Te,x1Te,affine,localMaps,addDim = dataSets.dataRegress(typeData, NTr, NTe, affine, localMaps, addDim, dct, sparseProj)

    # if preDimRed:
        # modify datasets with some dimension reduction technique, and set withNormals accordingly in call to pointSetMatching

    # Normalize training and testing data so median value of (|x_k-x_l|^2, k.l = 1,...,NTr) of training set is equal to 1
    dst = np.sqrt(((x0Tr[:, np.newaxis, :] - x0Tr[np.newaxis,:,:])**2).sum(axis=2))
    sigma = np.percentile(dst[np.tril_indices(NTr)], 50)
    print('Estimated sigma:', sigma)
    x0Tr /= sigma
    x0Te /= sigma
    sigma = 1.0

    # Linear regression training (least-squares)
    fu = pointSets.learnRegression(x0Tr, x1Tr)
    # while np.fabs(fu).max() < 1e-8:
    #     # If fu values too small, re-train with learnLogistic and smaller l1Cost
    #     l1Cost *= 0.9
    #     fu = pointSets.learnRegression(x0Tr, x1Tr, w=wTr, l1Cost=l1Cost)

    # xDef1 = x0Te with column of one's added to left
    xDef1 = np.concatenate((np.ones((x0Te.shape[0], 1)), x0Te), axis=1)

    # Linear regression results on test dataset
    gu = np.dot(xDef1, fu)

    # Remove columns of x0Tr and x0Te corresponding to null vectors (i.e., dimensions) in fu
    if removeNullDirs:
        J = np.nonzero(np.fabs(fu[1:-1, :]).sum(axis=1) > 1e-8)[0]
        x0Tr0 = x0Tr[:,J]
        x0Te0 = x0Te[:,J]
    else:
        x0Tr0 = x0Tr
        x0Te0 = x0Te

    # Create object f from the class PointSetMatching and initialize the attributes of the class
    K1 = kfun.Kernel(name='laplacian', sigma=sigma, order=1)
    sm = PointSetMatchingParam(timeStep=0.1, KparDiff = K1, sigmaError=sigError*np.sqrt(NTr), errorType='regression')
    f = PointSetMatching(trainingSet= (x0Tr0,x1Tr), outputDir=outputDir, param=sm, regWeight=1.,
                         saveTrajectories=True, pplot=True, testSet=(x0Te0, x1Te), addDim = addDim, u0=u0,
                         normalizeInput=False, l1Cost = l1Cost, relearnRate=relearnRate, randomInit=randomInit,
                         affine=affine, testGradient=True, affineWeight=10.,
                         maxIter=1500, plotDim=plotDim, verbose=verbose)

    # Create object testInit from the class RegressTestErrors and initialize the attributes of the class
    testInit = RegressTestErrors()

    # Linear regression error (MSE) for original test dataset
    testInit.leastSq = ((gu - x1Te)**2).sum() / x1Te.shape[0]

    # linSVM and SVM training, testing, and error calculations (on original training and test datasets)
    testInit = trainTest.regressTrainTest(testInit, f, x0Tr, x1Tr, x0Te, x1Te, 0)

    print(testInit)

    # if localMaps is not None:
    #     K1.localMaps = localMaps
    #
    for k in range(1):
         f.optimizeMatching()

    return f, testInit


def dimRed(typeData, l1Cost = 1.0, addDim = 1, sigError = 1., randomInit=0.05, removeNullDirs = False,
             dct = False, NTr = 100, NTe = 2000, outputDir = '.'):

    return f, testInit


if __name__ == "__main__":

    if diffeoLearn == 'classif':
        # AllTD = {'helixes3':(100,), 'helixes10':(100,200,500,1000),
        #          'helixes20':(100,200,500,1000), 'Dolls':(100,200,500,1000),
        #          'Segments11':(100,200,500,1000), 'TwoSegments':(100,200,500,1000),'TwoSegmentsCumSum':(100,200,500,1000), 'RBF':(100,200,500,1000)}
        #AllTD = {'helixes3': (100,)}
        #AllTD = {'helixes10': (200,)}
        #AllTD = {'helixes20': (200,)}
        #AllTD = {'Dolls': (500,)}
        #AllTD = {'Segments11': (200,)}
        #AllTD = {'TwoSegments': (100,)}
        #AllTD = {'TwoSegmentsCumSum': (200,)}
        #AllTD = {'RBF': (100,)}
        #AllTD = {'Line': (100,)}
        #typeData = 'Dolls'

        outputDir0 = outputDirPath+'/Results/Classif'
        loggingUtils.setup_default_logging(outputDir0, fileName='info', stdOutput=True)
        #f, testInit = Classify('TwoSegments', l1Cost=1., addDim=1, sigError=0.01, randomInit=0.05, removeNullDirs=False, NTr=200,
        #                       NTe=2000, outputDir=outputDir0)

        for typeData in AllTD.keys():
            for NTr in AllTD[typeData]:
                print(typeData, 'NTr = ', NTr)
                outputDir = outputDir0+'/'+typeData+'_{0:d}'.format(NTr)
                f,testInit = classify(typeData, l1Cost = 1., addDim = addDim, sigError = .1, randomInit=0.05, removeNullDirs = False,
                                      NTr = NTr, NTe = 2000, outputDir=outputDir)

                with open(outputDir+'/results.txt','a+') as fl:
                    fl.write('\n'+typeData+' dim = {0:d} N = {1:d}\n'.format(f.fv0.shape[1], f.fv0.shape[0]))

                    fl.write('Initial: '+testInit.__repr__())
                    fl.write('Final: ' +f.testError.__repr__())

                # if typeData=='MNIST':
                #     pca.inverse_transform(f.fvDef[:,0:f.fvDef.shape[1]- addDim]).tofile(outputDir + '/mnistOutTrainDef.txt')
                #     pca.inverse_transform(f.testDef[:,0:f.fvDef.shape[1]- addDim]).tofile(outputDir + '/mnistOutTestDef.txt')
                #

                #plt.pause(100)

    elif diffeoLearn == 'regress':
        #AllTD = {'helixes3': (100,)}
        outputDir0 = outputDirPath+'/Results/Regress'
        loggingUtils.setup_default_logging(outputDir0, fileName='info', stdOutput=True)
        # f, testInit = regress('TwoSegments', l1Cost=1., addDim=1, sigError=0.01, randomInit=0.05, removeNullDirs=False, NTr=200,
        #                       NTe=2000, outputDir=outputDir0)
        for typeData in AllTD.keys():
            for NTr in AllTD[typeData]:
                print(typeData, 'NTr = ', NTr)
                outputDir = outputDir0 + '/' + typeData + '_{0:d}'.format(NTr)
                f, testInit = regress(typeData, l1Cost=1., addDim=addDim, sigError=.1, randomInit=0.05, removeNullDirs=False,
                                      NTr=NTr, NTe=2000, outputDir=outputDir)

                fig = plt.figure(2)
                fig.clf()
                plt.hist(f.fv1, bins='auto')
                plt.title('Training Dataset')
                plt.xlabel('Truth Values')
                fig = plt.figure(3)
                fig.clf()
                plt.hist(f.testSet[1], bins='auto')
                plt.title('Test Dataset')
                plt.xlabel('Truth Values')
                fig = plt.figure(4)
                fig.clf()
                plt.hist(f.guTr, bins='auto')
                plt.title('Training Dataset')
                plt.xlabel('Predicted Values')
                fig = plt.figure(5)
                fig.clf()
                plt.hist(f.guTe, bins='auto')
                plt.title('Test Dataset')
                plt.xlabel('Predicted Values')

                print('Number of guTr points < -1.2 = ', (f.guTr<-1.2).sum())
                print('Number of guTr points > 1.2 = ', (f.guTr>1.2).sum())
                print('Number of guTe points < -1.2 = ', (f.guTe<-1.2).sum())
                print('Number of guTe points > 1.2 = ', (f.guTe>1.2).sum())

                with open(outputDir + '/results.txt', 'a+') as fl:
                    fl.write('\n' + typeData + ' dim = {0:d} N = {1:d}\n'.format(f.fv0.shape[1], f.fv0.shape[0]))

                    fl.write('Initial: ' + testInit.__repr__())
                    fl.write('Final: ' + f.testError.__repr__())

    elif diffeoLearn == 'dimRed':
        AllTD = {'helixes10': (200,)}
        AllTD = {'Line': (100,)}
        # typeData = 'Dolls'

        outputDir0 = outputDirPath+'/Results/DimReduction'
        loggingUtils.setup_default_logging(outputDir0, fileName='info', stdOutput=True)
        # f, testInit = dimRed('TwoSegments', l1Cost=1., addDim=1, sigError=0.01, randomInit=0.05, removeNullDirs=False, NTr=200,
        #                       NTe=2000, outputDir=outputDir0)
        for typeData in AllTD.keys():
            for NTr in AllTD[typeData]:
                print(typeData, 'NTr = ', NTr)
                outputDir = outputDir0 + '/' + typeData + '_{0:d}'.format(NTr)
                f, testInit = dimRed(typeData, l1Cost=1., addDim=1, sigError=.01, randomInit=0.05, removeNullDirs=False,
                                     NTr=NTr, NTe=2000, outputDir=outputDir)

                with open(outputDir + '/results.txt', 'a+') as fl:
                    fl.write('\n' + typeData + ' dim = {0:d} N = {1:d}\n'.format(f.fv0.shape[1], f.fv0.shape[0]))

                    fl.write('Initial: ' + testInit.__repr__())
                    fl.write('Final: ' + f.testError.__repr__())

    print('\n \n Close figure to end script \n ')
    plt.show(block=True)
