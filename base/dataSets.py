import numpy as np
import scipy.linalg as la
import scipy.fftpack as fft
import scipy.stats as stats


def dataRegress(typeData, NTr, NTe, affine, localMaps, addDim, dct, sparseProj):

    localMaps = None
    affine = 'none'
    if typeData in ('helixes3', 'helixes10', 'helixes20', 'Line', 'TwoSegments', 'RBF'):

        x0Tr, x1Tr, x0Te, x1Te, affine, localMaps, addDim = dataClassif(typeData, NTr, NTe, affine, localMaps, addDim, dct, sparseProj)

        x1Tr = 0.8 * x1Tr + 0.2
        x1Te = 0.8 * x1Te + 0.2

    elif typeData in ('Dolls'):

        d = 3
        x0Tr = np.random.multivariate_normal(np.zeros(d), np.eye(d), NTr)
        x0Te = np.random.multivariate_normal(np.zeros(d), np.eye(d), NTe)
        nrm = np.sqrt((x0Tr ** 2).sum(axis=1))
        x1Tr = np.array(np.cos(4 * nrm))[:, np.newaxis]
        # x1Tr = np.array(np.sign(np.cos(4 * nrm)) > 0, dtype=int)[:, np.newaxis]
        nrm = np.sqrt((x0Te ** 2).sum(axis=1))
        x1Te = np.array(np.cos(4 * nrm))[:, np.newaxis]
        # x1Te = np.array(np.sign(np.cos(4 * nrm)) > 0, dtype=int)[:, np.newaxis]
        # x1Tr = (x1Tr + 1)/2
        # x1Te = (x1Te + 1)/2

        if dct:
            for k in range(2 * NTr):
                x0Tr[k, :] = fft.dct(x0Tr[k, :]) / np.sqrt(d)
            for k in range(2 * NTe):
                x0Te[k, :] = fft.dct(x0Te[k, :]) / np.sqrt(d)

        if sparseProj:
            A = 2 * (np.random.random((d, d)) > 0.9) - 1
            x0Tr = np.dot(x0Tr, A)
            x0Te = np.dot(x0Te, A)


    return x0Tr, x1Tr, x0Te, x1Te, affine, localMaps, addDim


def dataClassif(typeData, NTr, NTe, affine, localMaps, addDim, dct, sparseProj):

    localMaps = None
    affine = 'none'
    if typeData in ('helixes3', 'helixes10', 'helixes20'):
        if typeData == 'helixes3':
            d = 3
        elif typeData == 'helixes10':
            d = 10
        else:
            d = 20

        h = 0.25
        x0Tr = 0.05 * np.random.randn(2 * NTr, d)
        x0Te = 0.05 * np.random.randn(2 * NTe, d)
        # x1 = np.random.randn(100,2)
        x1Tr = np.ones((2 * NTr, 1), dtype=int)
        x1Te = np.ones((2 * NTe, 1), dtype=int)
        x1Tr[NTr:2 * NTr] = 0
        x1Te[NTe:2 * NTe] = 0
        t = 2 * np.pi * np.random.rand(NTr)
        s = 2 * np.pi * np.random.rand(NTr)
        x0Tr[0:NTr, 0] += np.cos(t) + h * np.cos(s)
        x0Tr[0:NTr, 1] += np.sin(t) + h * np.cos(s)
        x0Tr[0:NTr, 2] += h * np.sin(s)

        t = 2 * np.pi * np.random.rand(NTe)
        s = 2 * np.pi * np.random.rand(NTe)
        x0Te[0:NTe, 0] += np.cos(t) + h * np.cos(s)
        x0Te[0:NTe, 1] += np.sin(t) + h * np.cos(s)
        x0Te[0:NTe, 2] += h * np.sin(s)

        t = 2 * np.pi * np.random.rand(NTr)
        s = 2 * np.pi * np.random.rand(NTr)
        x0Tr[NTr:2 * NTr, 0] += h * np.sin(s)
        x0Tr[NTr:2 * NTr, 1] += 1 + np.cos(t) + h * np.cos(s)
        x0Tr[NTr:2 * NTr, 2] += np.sin(t) + h * np.cos(s)

        t = 2 * np.pi * np.random.rand(NTe)
        s = 2 * np.pi * np.random.rand(NTe)
        x0Te[NTe:2 * NTe, 0] += h * np.sin(s)
        x0Te[NTe:2 * NTe, 1] += 1 + np.cos(t) + h * np.cos(s)
        x0Te[NTe:2 * NTe, 2] += np.sin(t) + h * np.cos(s)

        x0Tr[:, 3:d] += 1. * np.random.randn(2 * NTr, d - 3)
        x0Te[:, 3:d] += 1. * np.random.randn(2 * NTe, d - 3)
        A = np.random.randn(d, d)
        R = la.expm((A - A.T) / 2)
        x0Tr = np.dot(x0Tr, R)
        x0Te = np.dot(x0Te, R)
        affine = 'euclidean'
    elif typeData == 'csv1':
        nv = -1
        X = np.genfromtxt('/Users/younes/Development/Data/Classification/BRCA1_q2_HW2.csv', delimiter=',')
        x1 = np.array(X[0, :].T, dtype=int)
        x0 = X[1:nv, :].T
        for k in range(x0.shape[0]):
            x0[k, :] = stats.rankdata(np.ravel(x0[k, :]), method='average') / float(x0.shape[1])
        I1 = np.nonzero(x1 == 1)[0]
        I2 = np.nonzero(x1 == 0)[0]
        J1 = np.random.random(I1.size)
        J2 = np.random.random(I2.size)
        x0Tr = np.concatenate((x0[I1[J1 > 0.33], :], x0[I2[J2 > 0.33], :]))
        x0Te = np.concatenate((x0[I1[J1 < 0.33], :], x0[I2[J2 < 0.33], :]))
        NTr = x0Tr.shape[0]
        NTe = x0Te.shape[0]
        x1Tr = np.concatenate((x1[I1[J1 > 0.33]], x1[I2[J2 > 0.33]]))[:, np.newaxis]
        x1Te = np.concatenate((x1[I1[J1 < 0.33]], x1[I2[J2 < 0.33]]))[:, np.newaxis]
        d = x0Tr.shape[1]
    elif typeData == 'RBF':
        d = 10
        d1 = 10
        nc = 20
        # centers = np.random.normal(0, 1, (nc, d))
        centers = np.zeros((nc, d))
        c = np.zeros(nc)
        for k in range(nc):
            centers[k, k % d] = 0.5 * k / float(nc)
            c[k] = (2 * (k % 1.5) - 0.75)
        # c = (2*np.random.random(nc) -1)
        x0Tr = np.random.normal(0, 1, (NTr, d))
        K = np.exp(- ((x0Tr[:, np.newaxis, :] - centers[np.newaxis, :, :]) ** 2).sum(axis=2) / np.sqrt(d))
        m = np.median(np.sin(np.dot(K, c)))
        x0Tr = np.random.normal(0, 1, (NTr, d))
        K = np.exp(- ((x0Tr[:, np.newaxis, :] - centers[np.newaxis, :, :]) ** 2).sum(axis=2) / np.sqrt(d))
        x1Tr = np.array((np.sin(np.dot(K, c)) - m)[:, np.newaxis] > 0, dtype=int)
        x0Te = np.random.normal(0, 1, (NTe, d))
        K = np.exp(- ((x0Te[:, np.newaxis, :] - centers[np.newaxis, :, :]) ** 2).sum(axis=2) / np.sqrt(d))
        x1Te = np.array((np.sin(np.dot(K, c)) - m)[:, np.newaxis] > 0, dtype=int)
        # x0Tr = np.concatenate((x0Tr,0.5*np.random.normal(0,1,(NTr,d1))), axis=1)
        # x0Te = np.concatenate((x0Te,0.5*np.random.normal(0,1,(NTe,d1))), axis=1)
        # d += d1
        # localMaps = PointSetMatching().localMaps1D(d+1)

    elif typeData in ('MoG', 'MoGHN'):
        d = 10
        if typeData == 'MoGHN':
            cn = 10.
        else:
            cn = 1.
        Cov0 = cn * np.eye(d)
        m0 = np.concatenate((np.ones(3), np.zeros(d - 3)))
        q = np.arange(0, 1, 1.0 / d)
        Cov1 = 2 * cn * np.exp(-np.abs(q[:, np.newaxis] - q[np.newaxis, :]))
        # Cov1 = np.eye(d)
        Cov2 = 2 * cn * np.exp(-np.abs(q[:, np.newaxis] - q[np.newaxis, :]) / 3.)
        # Cov2 = np.eye(d)
        m1 = np.concatenate((-np.ones(3), np.zeros(d - 3)))
        m2 = np.concatenate((-np.array([1, -1, 1]), np.zeros(d - 3)))
        x0Tr = np.zeros((3 * NTr, d))
        x0Te = np.zeros((3 * NTe, d))
        x0Tr[0:NTr, :] = np.random.multivariate_normal(m0, Cov0, size=NTr)
        x0Te[0:NTe, :] = np.random.multivariate_normal(m0, Cov0, size=NTe)
        x0Tr[NTr:2 * NTr, :] = np.random.multivariate_normal(m1, Cov1, size=NTr)
        x0Te[NTe:2 * NTe, :] = np.random.multivariate_normal(m1, Cov1, size=NTe)
        x0Tr[2 * NTr:3 * NTr, :] = np.random.multivariate_normal(m2, Cov2, size=NTr)
        x0Te[2 * NTe:3 * NTe, :] = np.random.multivariate_normal(m2, Cov2, size=NTe)
        x1Tr = np.zeros((3 * NTr, 1), dtype=int)
        x1Te = np.zeros((3 * NTe, 1), dtype=int)
        x1Tr[NTr:2 * NTr] = 1
        x1Te[NTe:2 * NTe] = 1
        x1Tr[2 * NTr:3 * NTr] = 2
        x1Te[2 * NTe:3 * NTe] = 2
        A = np.random.randn(d, d)
        R = la.expm((A - A.T) / 2)
        x0Tr = np.dot(x0Tr, R)
        x0Te = np.dot(x0Te, R)
    elif typeData == 'MNIST':
        mndata = MNIST('/cis/home/younes/MNIST')
        images, labels = mndata.load_training()
        imTest, labTest = mndata.load_testing()
        d = len(images[0])
        cls = [3, 5, 8]
        x0Tr = np.zeros((NTr, d))
        x1Tr = np.zeros((NTr, 1), dtype=int)
        kk = 0
        for k in range(len(images)):
            if labels[k] in cls:
                x0Tr[kk, :] = np.array(images[k]) / 255.
                x1Tr[kk] = cls.index(labels[k])
                kk += 1
                if kk == NTr:
                    break
        if kk < NTr:
            NTr = kk
            x0Tr = x0Tr[0:NTr, :]
            x1Tr = x1Tr[0:NTr]
        # std = np.std(x0Tr, axis=0)
        # print sum(std > 0.05)
        # pca = PCA(n_components=0.90)
        # x0Tr = pca.fit_transform(x0Tr)
        # x0Tr = x0Tr / np.sqrt(pca.singular_values_)
        # x0Tr = x0Tr[:,std>0.05]

        x0Te = np.zeros((NTe, d))
        x1Te = np.zeros((NTe, 1), dtype=int)
        kk = 0
        for k in range(len(imTest)):
            if labTest[k] in cls:
                x0Te[kk, :] = np.array(imTest[k]) / 255.
                x1Te[kk] = cls.index(labTest[k])
                kk += 1
                if kk == NTe:
                    break
        if kk < NTe:
            NTe = kk
            x0Te = x0Te[0:NTe, :]
            x1Te = x1Te[0:NTe]
        # x0Te = pca.transform(x0Te) #/np.sqrt(pca.singular_values_)
        # x0Te = x0Te[:,std>0.05]
        # pca.inverse_transform(x0Tr).tofile(outputDir + '/mnistOutTrain.txt')
        # pca.inverse_transform(x0Te).tofile(outputDir + '/mnistOutTest.txt')
    elif typeData == 'Dolls':
        d = 3
        x0Tr = np.random.multivariate_normal(np.zeros(d), np.eye(d), NTr)
        x0Te = np.random.multivariate_normal(np.zeros(d), np.eye(d), NTe)
        nrm = np.sqrt((x0Tr ** 2).sum(axis=1))
        x1Tr = np.array(np.sign(np.cos(4 * nrm)) > 0, dtype=int)[:, np.newaxis]
        nrm = np.sqrt((x0Te ** 2).sum(axis=1))
        x1Te = np.array(np.sign(np.cos(4 * nrm)) > 0, dtype=int)[:, np.newaxis]
    elif typeData == 'Segments11' or typeData == 'Segments12':
        d = 100
        l0 = 10
        if typeData == 'Segments11':
            l1 = 11
        else:
            l1 = 12
        x0Tr = np.zeros((2 * NTr, d))
        x1Tr = np.zeros((2 * NTr, 1), dtype=int)
        x1Tr[NTr:2 * NTr, 0] = 1
        start = np.random.randint(0, d, NTr)
        for k in range(NTr):
            x0Tr[k, np.arange(start[k], start[k] + l0) % d] = 1
        start = np.random.randint(0, d, NTr)
        for k in range(NTr):
            x0Tr[k + NTr, np.arange(start[k], start[k] + l1) % d] = 1
        x0Te = np.zeros((2 * NTe, d))
        x1Te = np.zeros((2 * NTe, 1), dtype=int)
        x1Te[NTe:2 * NTe, 0] = 1
        start = np.random.randint(0, d, NTe)
        for k in range(NTe):
            x0Te[k, np.arange(start[k], start[k] + l0) % d] = 1
        start = np.random.randint(0, d, NTe)
        for k in range(NTe):
            x0Te[k + NTe, np.arange(start[k], start[k] + l1) % d] = 1
        # x0Tr += 0.01 * np.random.randn(x0Tr.shape[0], x0Tr.shape[1])
        # x0Te += 0.01 * np.random.randn(x0Te.shape[0], x0Te.shape[1])
        x0Tr *= (1 + 0.25 * np.random.randn(x0Tr.shape[0], 1))
        x0Te *= (1 + 0.25 * np.random.randn(x0Te.shape[0], 1))

        # localMaps = PointSetMatching().localMaps1D(d)
    elif typeData in ('TwoSegments', 'TwoSegmentsCumSum'):
        d = 100
        # x0Tr = np.random.normal(0, 0.000001, (2 * NTr, d))
        x0Tr = np.zeros((2 * NTr, d))
        x1Tr = np.zeros((2 * NTr, 1), dtype=int)
        x1Tr[NTr:2 * NTr, 0] = 1
        start = np.zeros((NTr, 2), dtype=int)
        start[:, 0] = np.random.randint(0, d, NTr)
        start[:, 1] = start[:, 0] + 5 + np.random.randint(0, d - 11, NTr)
        for k in range(NTr):
            x0Tr[k, np.arange(start[k, 0], start[k, 0] + 4) % d] = 1
            x0Tr[k, np.arange(start[k, 1], start[k, 1] + 6) % d] = 1
            # x0Tr[k, np.array([start[k,0], start[k,0] + 3])%d] = 1
            # x0Tr[k, np.array([start[k, 0]+start[k,1]+5, start[k, 0]+start[k,1] + 10])%d] = 1

        start[:, 0] = np.random.randint(0, d, NTr)
        start[:, 1] = start[:, 0] + 6 + np.random.randint(0, d - 11, NTr)
        for k in range(NTr):
            x0Tr[k + NTr, np.arange(start[k, 0], start[k, 0] + 5) % d] = 1
            x0Tr[k + NTr, np.arange(start[k, 1], start[k, 1] + 5) % d] = 1
            # x0Tr[k+NTr, np.array([start[k, 0], start[k, 0] + 4])%d] = 1
            # x0Tr[k+NTr, np.array([start[k, 0] + start[k, 1] + 6, start[k, 0] + start[k, 1] + 10])%d] = 1

        # x0Te = np.random.normal(0, 0.000001, (2 * NTe, d))
        x0Te = np.zeros((2 * NTe, d))
        # x0Te = np.zeros((2 * NTe, d))
        x1Te = np.zeros((2 * NTe, 1), dtype=int)
        x1Te[NTe:2 * NTe, 0] = 1
        start = np.zeros((NTe, 2), dtype=int)
        start[:, 0] = np.random.randint(0, d, NTe)
        start[:, 1] = start[:, 0] + 5 + np.random.randint(0, d - 11, NTe)
        for k in range(NTe):
            x0Te[k, np.arange(start[k, 0], start[k, 0] + 4) % d] = 1
            x0Te[k, np.arange(start[k, 1], start[k, 1] + 6) % d] = 1

        start[:, 0] = np.random.randint(0, d, NTe)
        start[:, 1] = start[:, 0] + 6 + np.random.randint(0, d - 11, NTe)
        for k in range(NTe):
            x0Te[k + NTe, np.arange(start[k, 0], start[k, 0] + 5) % d] = 1
            x0Te[k + NTe, np.arange(start[k, 1], start[k, 1] + 5) % d] = 1
            # x0Te[k+NTe, np.array([start[k, 0], start[k, 0] + 4])%d] = 1
            # x0Te[k+NTe, np.array([start[k, 0] + start[k, 1] + 6, start[k, 0] + start[k, 1] + 10])%d] = 1

        # x0Tr *= (1 + 0.25 * np.random.randn(x0Tr.shape[0], 1))
        # x0Te *= (1 + 0.25 * np.random.randn(x0Te.shape[0], 1))
        dct = False
        sparseProj = False
        if typeData == 'TwoSegmentsCumSum':
            x0Tr = np.cumsum(x0Tr, axis=1)
            x0Te = np.cumsum(x0Te, axis=1)
        # A = np.random.normal(0,1,(d,5))
        # x0Tr = np.concatenate((x0Tr, np.dot(x0Tr, A)), axis=1)
        # x0Te = np.concatenate((x0Te, np.dot(x0Te, A)), axis=1)

        # localMaps = PointSetMatching().localMapsCircle(d+1)
    elif typeData == 'maxGauss':
        d = 10
        x0Tr = np.random.normal(0, 1, (2 * NTr, d))
        M = np.max(x0Tr, axis=1)
        # u = d/np.sqrt(2*np.pi)
        # T = np.sqrt(2*np.log(u) - np.log(2*np.log(u)))
        # T = 2.46#d=100
        T = 1.495  # d=10
        x1Tr = np.array(M > T, dtype=int)[:, np.newaxis]
        x0Te = np.random.normal(0, 1, (2 * NTe, d))
        M = np.max(x0Te, axis=1)
        x1Te = np.array(M > T, dtype=int)[:, np.newaxis]
        A = np.random.randn(d, d)
        R = la.expm((A - A.T) / 2)
        x0Tr = np.dot(x0Tr, R)
        x0Te = np.dot(x0Te, R)
    elif typeData == 'Line':
        d = 10
        x0Tr = np.zeros((2 * NTr, d))
        x0Tr[NTr:2 * NTr, :] = np.random.normal(0, 1, (NTr, d))
        t = np.random.normal(0, 1, (NTr,))
        x0Tr[0:NTr, 0] = np.cos(2 * np.pi * t)
        x0Tr[0:NTr, 1] = np.sin(2 * np.pi * t)
        x0Tr[0:NTr, 2] = t
        x0Te = np.zeros((2 * NTe, d))
        x0Te[NTe:2 * NTe, :] = np.random.normal(0, 1, (NTe, d))
        t = np.random.normal(0, 1, (NTe,))
        x0Te[0:NTe, 0] = np.cos(2 * np.pi * t)
        x0Te[0:NTe, 1] = np.sin(2 * np.pi * t)
        x0Te[0:NTe, 2] = t
        x1Tr = np.zeros((2 * NTr, 1), dtype=int)
        x1Tr[NTr:2 * NTr, 0] = 1
        x1Te = np.zeros((2 * NTe, 1), dtype=int)
        x1Te[NTe:2 * NTe, 0] = 1
        A = np.random.randn(d, d)
        R = la.expm((A - A.T) / 2)
        x0Tr = np.dot(x0Tr, R)
        x0Te = np.dot(x0Te, R)
    elif typeData == 'xor':
        d = 50
        x0Tr = np.zeros((2 * NTr, d))
        x1Tr = np.zeros((2 * NTr, 1), dtype=int)
        for k in range(NTr):
            a = np.random.permutation(d)
            x0Tr[k, a[0]] = 2 * np.random.randint(0, 2) - 1
            x0Tr[k, a[1]] = x0Tr[k, a[0]]
            x1Tr[k] = 0
        for k in range(NTr):
            a = np.random.permutation(d)
            x0Tr[k + NTr, a[0]] = 2 * np.random.randint(0, 2) - 1
            x0Tr[k + NTr, a[1]] = -x0Tr[k + NTr, a[0]]
            x1Tr[k + NTr] = 1
        x0Te = np.zeros((2 * NTe, d))
        x1Te = np.zeros((2 * NTe, 1), dtype=int)
        for k in range(NTe):
            a = np.random.permutation(d)
            x0Te[k, a[0]] = 2 * np.random.randint(0, 2) - 1
            x0Te[k, a[1]] = x0Te[k, a[0]]
            x1Te[k] = 0
        for k in range(NTe):
            a = np.random.permutation(d)
            x0Te[k + NTe, a[0]] = 2 * np.random.randint(0, 2) - 1
            x0Te[k + NTe, a[1]] = -x0Te[k + NTe, a[0]]
            x1Te[k + NTe] = 1
        # affine = 'euclidean'
        addDim = 1

        # localMaps = PointSetMatching().localMapsNaive(d)
    elif typeData == 'sigmoid':
        d = 50
        beta1 = 0.1
        beta2 = 0.11
        x0Tr = np.zeros((2 * NTr, d))
        x1Tr = np.zeros((2 * NTr, 1), dtype=int)
        t = np.arange(0, 1, 1. / d)
        a = np.random.rand(NTr)
        x0Tr[0:NTr, :] = np.log(np.cosh((t[np.newaxis, :] - a[:, np.newaxis]) / beta1))
        a = np.random.rand(NTr)
        x0Tr[NTr:2 * NTr, :] = np.log(np.cosh((t[np.newaxis, :] - a[:, np.newaxis]) / beta2))
        x1Tr[NTr:2 * NTr] = 1

        x0Te = np.zeros((2 * NTe, d))
        x1Te = np.zeros((2 * NTe, 1), dtype=int)
        t = np.arange(0, 1, 1. / d)
        a = np.random.rand(NTe)
        x0Te[0:NTe, :] = np.log(np.cosh((t[np.newaxis, :] - a[:, np.newaxis]) / beta1))
        a = np.random.rand(NTe)
        x0Te[NTe:2 * NTe, :] = np.log(np.cosh((t[np.newaxis, :] - a[:, np.newaxis]) / beta2))
        x1Te[NTe:2 * NTe] = 1
        # addDim = 0
        # localMaps = PointSetMatching().localMaps1D(d)

    else:
        d = 10
        Cov0 = 2 * np.eye(d)
        m0 = np.concatenate((np.ones(3), np.zeros(d - 3)))
        q = np.arange(0, 1, 1.0 / d)
        Cov1 = 2 * np.exp(-np.abs(q[:, np.newaxis] - q[np.newaxis, :]))
        # Cov1 = np.eye(d)
        m1 = np.concatenate((-np.ones(3), np.zeros(d - 3)))
        x0Tr = np.zeros((2 * NTr, d))
        x0Te = np.zeros((2 * NTe, d))
        x0Tr[0:NTr, :] = np.random.multivariate_normal(m0, Cov0, size=NTr)
        x0Te[0:NTe, :] = np.random.multivariate_normal(m0, Cov0, size=NTe)
        x0Tr[NTr:2 * NTr, :] = np.random.multivariate_normal(m1, Cov1, size=NTr)
        x0Te[NTe:2 * NTe, :] = np.random.multivariate_normal(m1, Cov1, size=NTe)
        x1Tr = np.ones((2 * NTr, 1), dtype=int)
        x1Te = np.ones((2 * NTe, 1), dtype=int)
        x1Tr[NTr:2 * NTr] = 0
        x1Te[NTe:2 * NTe] = 0

    if dct:
        for k in range(2 * NTr):
            x0Tr[k, :] = fft.dct(x0Tr[k, :])/np.sqrt(d)
        for k in range(2 * NTe):
            x0Te[k, :] = fft.dct(x0Te[k, :])/np.sqrt(d)

    if sparseProj:
        A = 2*(np.random.random((d,d)) > 0.9) - 1
        x0Tr = np.dot(x0Tr, A)
        x0Te = np.dot(x0Te, A)

    return x0Tr, x1Tr, x0Te, x1Te, affine, localMaps, addDim
