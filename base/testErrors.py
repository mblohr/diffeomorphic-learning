
class RegressTestErrors:
    def __init__(self):
        self.leastSq = 1.
        self.SVM = 1.
        self.linSVM = 1.
        self.rbfSVM = 1.
    def __repr__(self):
        rates = 'leastSq: {0:f}, SVM: {1:f}, linSVM: {2:f}, rbfSVM: {3:f}\n'.format(self.leastSq, self.SVM, self.linSVM, self.rbfSVM)
        return rates

class ClassTestErrors:
    def __init__(self):
        self.knn = 1.
        self.SVM = 1.
        self.linSVM = 1.
        self.RF = 1.
        self.logistic = 1.
        self.mlp = 1.
        self.GnB = 1.
    def __repr__(self):
        rates = 'logistic: {0:f}, SVM: {1:f}, linSVM: {2:f}, RF: {3:f}, kNN: {4:f}, MLP: {5:f}, GnB: {6:f}\n'.format(self.logistic,
                                        self.SVM, self.linSVM, self.RF, self.knn, self.mlp, self.GnB)
        return rates

class DimRedTestErrors:
    def __init__(self):
        self.knn = 1.
        self.SVM = 1.
        self.linSVM = 1.
        self.RF = 1.
        self.logistic = 1.
        self.mlp = 1.
    def __repr__(self):
        rates = 'logistic: {0:f}, SVM: {1:f}, linSVM: {2:f}, RF: {3:f}, kNN: {4:f}, MLP: {5:f}\n'.format(self.logistic,
                                        self.SVM, self.linSVM, self.RF, self.knn, self.mlp)
        return rates

