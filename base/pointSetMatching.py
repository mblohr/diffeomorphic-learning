import os
import numpy as np
import logging
import scipy.linalg as la
from .testErrors import ClassTestErrors, RegressTestErrors
from . import conjugateGradient as cg, kernelFunctions as kfun, pointEvolution as evol, bfgs
from . import pointSets
from .affineBasis import AffineBasis, getExponential
from . import trainTest
from .plotData import plotClassRegress
from sklearn.decomposition import PCA


## Parameter class for matching
#      timeStep: time discretization
#      KparDiff: object kernel: if not specified, use typeKernel with width sigmaKernel
#      KparDist: kernel in current/measure space: if not specified, use gauss kernel with width sigmaDist  #?
#      sigmaError: normalization for error term
#      errorType: 'measure' or 'current'
#      typeKernel: 'gauss' or 'laplacian' #?
class PointSetMatchingParam:
    def __init__(self, timeStep = .1, KparDiff = None, sigmaKernel = 6.5, sigmaError = 1.0, errorType = 'L2'):
        self.timeStep = timeStep
        self.sigmaKernel = sigmaKernel
        self.sigmaError = sigmaError
        self.errorType = errorType
        self.fun_obj = None
        self.fun_objGrad = None
        #self.errorType = 'classification' # commented out
        if KparDiff is None:
            self.KparDiff = kfun.Kernel(name = 'gauss', sigma = self.sigmaKernel)
        else:
            self.KparDiff = KparDiff

class Direction:
    def __init__(self):
        self.diff = []
        self.aff = []


## Main class for surface matching
#        Template: surface class (from surface.py); if not specified, opens fileTemp
#        Target: surface class (from surface.py); if not specified, opens fileTarg
#        param: surfaceMatchingParam
#        verb: verbose printing
#        regWeight: multiplicative constant on object regularization
#        affineWeight: multiplicative constant on affine regularization
#        rotWeight: multiplicative constant on affine regularization (supercedes affineWeight)
#        scaleWeight: multiplicative constant on scale regularization (supercedes affineWeight)
#        transWeight: multiplicative constant on translation regularization (supercedes affineWeight)
#        testGradient: evaluates gradient accuracy over random direction (debug)
#        outputDir: where results are saved
#        saveFile: generic name for saved surfaces
#        affine: 'affine', 'similitude', 'euclidean', 'translation' or 'none'
#        maxIter: max iterations in conjugate gradient
class PointSetMatching(object):

    def __init__(self, trainingSet, param=None, maxIter=1000,
                 regWeight = 1.0, affineWeight = 1.0, verb=True, testSet = None, addDim = 0, intercept=True,
                 u0 = None, normalizeInput=False, l1Cost = 0.0, relearnRate = 1,
                 rotWeight = None, scaleWeight = None, transWeight = None, randomInit = 0.,
                 testGradient=True, saveFile = 'evolution',
                 saveTrajectories = False, affine = 'none', outputDir = '.',pplot=True, plotDim=2, verbose=False):

        if param is None:
            self.param = PointSetMatchingParam()
        else:
            self.param = param

        self.fv0 = np.copy(trainingSet[0])
        self.fv1 = np.copy(trainingSet[1])
        self.plotDim = plotDim
        self.verbose = verbose

            #print np.fabs(self.fv1.surfel-self.fv0.surfel).max()

        if self.param.errorType == 'classification':
            if normalizeInput: # Currently set to False
                s = 1e-5 + np.std(self.fv0, axis=0)
                self.fv0 /= s
            if addDim > 0: # self.fv0 = x0Tr with addDim
                self.fv0 = np.concatenate((self.fv0,
                                           0.01*np.random.normal(size=(self.fv0.shape[0],addDim))), axis=1)
        elif self.param.errorType == 'regression':
            if normalizeInput: # Currently set to False
                s = 1e-5 + np.std(self.fv0, axis=0)
                self.fv0 /= s
            if addDim > 0: # self.fv0 = x0Tr with addDim
                self.fv0 = np.concatenate((self.fv0,
                                           0.01*np.random.normal(size=(self.fv0.shape[0],addDim))), axis=1)

        if verbose:
            print('addDim =', addDim)
        self.saveRate = 10
        self.relearnRate = relearnRate
        self.iter = 0
        self.setOutputDir(outputDir)
        self.dim = self.fv0.shape[1]
        self.maxIter = maxIter
        self.verb = verb
        self.testGradient = testGradient
        self.regweight = regWeight
        self.affine = affine
        self.affB = AffineBasis(self.dim, affine)
        self.affineDim = self.affB.affineDim
        self.affineBasis = self.affB.basis
        self.affineWeight = affineWeight * np.ones([self.affineDim, 1])
        if (len(self.affB.rotComp) > 0) & (rotWeight != None):
            self.affineWeight[self.affB.rotComp] = rotWeight
        if (len(self.affB.simComp) > 0) & (scaleWeight != None):
            self.affineWeight[self.affB.simComp] = scaleWeight
        if (len(self.affB.transComp) > 0) & (transWeight != None):
            self.affineWeight[self.affB.transComp] = transWeight


        self.x0 = np.copy(self.fv0)
        self.x0try = np.copy(self.fv0)
        self.fvDef = np.copy(self.fv0) # fvDef = fv0
        if verbose:
            print('self.fvDef.shape =', self.fvDef.shape)
        self.npt = self.fv0.shape[0]

        self.Tsize = int(round(1.0/self.param.timeStep))
        self.at = np.random.normal(0, randomInit, [self.Tsize, self.x0.shape[0], self.x0.shape[1]])
        self.atTry = np.zeros([self.Tsize, self.x0.shape[0], self.x0.shape[1]])
        self.Afft = np.zeros([self.Tsize, self.affineDim])
        self.AfftTry = np.zeros([self.Tsize, self.affineDim])
        self.xt = np.tile(self.x0, [self.Tsize+1, 1, 1])
        self.v = np.zeros([self.Tsize+1, self.npt, self.dim])
        self.obj = None
        self.objTry = None
        self.gradCoeff = self.x0.shape[0]
        self.saveFile = saveFile

        # Save training dataset (with addDim)
        pointSets.savePoints(self.outputDir + '/Template.vtk', self.fv0)

        self.coeffAff1 = 1.
        self.coeffAff2 = 100.
        self.coeffAff = self.coeffAff1
        self.coeffInitx = .1
        self.affBurnIn = 100
        self.pplot = pplot
        self.testSet = testSet
        self.l1Cost = l1Cost
        self.cgBurnIn = 100 #20

        if self.param.errorType == 'classification':
            self.intercept = intercept
            self.testError = ClassTestErrors()
            self.nclasses = self.fv1.max()+1
            nTr = np.zeros(self.nclasses)
            for k in range(self.nclasses):
                nTr[k] = (self.fv1==k).sum()
            #self.wtr = np.zeros(self.fv1.shape)
            self.wTr = float(self.fv1.size)/(nTr[self.fv1[:,0]]*self.nclasses)[:, np.newaxis]
            self.swTr = self.wTr.sum()
            self.rnd = 1.0
            self.coeffrnd = 0.99
            #self.wTr *= self.swTr

            if u0 is None:
                # Logistic regression training with self.fvDef and self.fv1
                self.u = self.learnLogistic()
            else:
                self.u = u0

            #print self.u
            print("Non-negative coefficients", (np.fabs(self.u).sum(axis=1) > 1e-10).sum())

            # xDef1 = fvDef, with column of one's added to left if intercept = True
            if self.intercept:
                xDef1 = np.concatenate((np.ones((self.fvDef.shape[0], 1)), self.fvDef), axis=1)
            else:
                xDef1 = self.fvDef

            # Logistic regression classification results and error for training dataset (with addDim, plus extra
            #                                                                            dim if intercept = True)
            gu = np.argmax(np.dot(xDef1, self.u), axis=1)[:,np.newaxis]
            self.guTr = np.copy(gu)
            err = np.sum(np.not_equal(gu, self.fv1)*self.wTr)/self.swTr
            logging.info('Training Error {0:0f}'.format(err))


            if self.testSet is not None:
                print('normalizeInput =', normalizeInput)
                if normalizeInput: # Currently set to False
                    self.testSet = (self.testSet[0]/s, self.testSet[1])
                if addDim > 0: # self.testSet[0] = x0Te with addDim
                    self.testSet = (np.concatenate((self.testSet[0], np.zeros((self.testSet[0].shape[0], addDim))),
                                                  axis=1),
                                   self.testSet[1])

                # Calculate wTe
                nTe = np.zeros(self.nclasses)
                for k in range(self.nclasses):
                    nTe[k] = (self.testSet[1] == k).sum()
                self.wTe = float(self.testSet[1].size)/(nTe[self.testSet[1][:, 0]]*self.nclasses)[:, np.newaxis]
                self.swTe = self.wTe.sum()
                #self.wTe *= self.swTe

                # testRes[0] = < # time points, # training data points, # dimensions (orig + addDim) >
                # testRes[1] = < # time points, # test data points,     # dimensions (orig + addDim) >
                testRes = evol.landmarkDirectEvolutionEuler(self.x0, self.at, self.param.KparDiff, withPointSet=self.testSet[0])

                # z_te(T) = xDef1 = testRes[1][-1,...], with column of one's added to left if intercept = True
                #         = phi(T,x_te) = transformed test dataset, where x_te = testSet[0]
                if self.intercept:
                    xDef1 = np.concatenate((np.ones((self.testSet[0].shape[0], 1)), testRes[1][-1,...]), axis=1)
                else:
                    xDef1 = testRes[1][-1, ...]

                # Logistic regression classification results and error for transformed test dataset z_te(T)
                gu = np.argmax(np.dot(xDef1, self.u), axis=1)[:,np.newaxis]
                self.guTe = np.copy(gu)
                test_err = np.sum(np.not_equal(gu, self.testSet[1])*self.wTe)/self.swTe
                logging.info('Testing Error {0:0f}'.format(test_err))

            if self.pplot:
                plotClassRegress(self, 1)

        elif self.param.errorType == 'regression':
            self.intercept = intercept
            self.testError = RegressTestErrors()
            self.rnd = 1.0
            self.coeffrnd = 0.99

            if u0 is None:
                # Linear regression training (least-squares) with self.fvDef and self.fv1
                self.u = pointSets.learnRegression(self.fvDef, self.fv1)
            else:
                self.u = u0

            #print self.u
            print("Non-negative coefficients", (np.fabs(self.u).sum(axis=1) > 1e-10).sum())

            # xDef1 = fvDef, with column of one's added to left
            xDef1 = np.concatenate((np.ones((self.fvDef.shape[0], 1)), self.fvDef), axis=1)

            # Linear regression results and error for training dataset (with addDim, plus extra dimension)
            gu = np.dot(xDef1, self.u)
            self.guTr = np.copy(gu)
            err = ((gu - self.fv1)**2).sum() / self.fv1.shape[0]
            logging.info('Training Error {0:0f}'.format(err))


            if self.testSet is not None:
                if normalizeInput: # Currently set to False
                    self.testSet = (self.testSet[0]/s, self.testSet[1])
                if addDim > 0: # self.testSet[0] = x0Te with addDim
                    self.testSet = (np.concatenate((self.testSet[0], np.zeros((self.testSet[0].shape[0], addDim))),
                                                  axis=1),
                                   self.testSet[1])

                # testRes[0] = < # time points, # training data points, # dimensions (orig + addDim) >
                # testRes[1] = < # time points, # test data points,     # dimensions (orig + addDim) >
                testRes = evol.landmarkDirectEvolutionEuler(self.x0, self.at, self.param.KparDiff, withPointSet=self.testSet[0])


                # z_te(T) = xDef1 = testRes[1][-1,...], with column of one's added to left
                #         = phi(T,x_te) = transformed test dataset, where x_te = testSet[0]
                xDef1 = np.concatenate((np.ones((self.testSet[0].shape[0], 1)), testRes[1][-1,...]), axis=1)

                # Linear regression results and error for transformed test dataset z_te(T)
                gu = np.dot(xDef1, self.u)
                self.guTe = np.copy(gu)
                test_err = ((gu - self.testSet[1]) ** 2).sum() / self.testSet[1].shape[0]

                logging.info('Testing Error {0:0f}'.format(test_err))

            if self.pplot:
                plotClassRegress(self, 1)

    def setOutputDir(self, outputDir):
        self.outputDir = outputDir
        if not os.access(outputDir, os.W_OK):
            if os.access(outputDir, os.F_OK):
                logging.error('Cannot save in ' + outputDir)
                return
            else:
                os.makedirs(outputDir)


    def dataTerm(self, _fvDef, _fvInit = None):
        if self.param.errorType == 'classification':
            obj = pointSets.LogisticScoreL2(_fvDef, self.fv1, self.u, w=self.wTr, intercept=self.intercept, l1Cost=self.l1Cost) \
                      / (self.param.sigmaError**2)
            #print('self.param.sigmaError =', self.param.sigmaError)
            # obj = pointSets.LogisticScore(_fvDef, self.fv1, self.u) / (self.param.sigmaError**2)
        elif self.param.errorType == 'regression':
            obj = pointSets.RegressionScore(_fvDef, self.fv1, self.u) / (self.param.sigmaError ** 2) #?
        if self.verbose:
            print('self.param.sigmaError =', self.param.sigmaError)
            print('dataTerm obj =', obj)
        return obj


    def objectiveFunDef(self, at, Afft, kernel = None, withTrajectory = False, withJacobian=False, x0 = None, regWeight = None):
        #print('objectiveFunDef')
        if x0 is None:
            x0 = self.x0
        if kernel is None:
            kernel = self.param.KparDiff
        #print 'x0 fun def', x0.sum()
        if regWeight is None:
            regWeight = self.regweight
        timeStep = 1.0/self.Tsize
        dim2 = self.dim**2
        A = [np.zeros([self.Tsize, self.dim, self.dim]), np.zeros([self.Tsize, self.dim])]
        foo = np.copy(self.fv0)
        if self.affineDim > 0:
            for t in range(self.Tsize):
                AB = np.dot(self.affineBasis, Afft[t])
                A[0][t] = AB[0:dim2].reshape([self.dim, self.dim])
                A[1][t] = AB[dim2:dim2+self.dim]
        if withJacobian:
            (xt,Jt)  = evol.landmarkDirectEvolutionEuler(x0, at, kernel, affine=A, withJacobian=True)
        else:
            xt  = evol.landmarkDirectEvolutionEuler(x0, at, kernel, affine=A)

        obj=0
        obj1 = 0 
        for t in range(self.Tsize):
            z = np.squeeze(xt[t, :, :])
            a = np.squeeze(at[t, :, :])
            #rzz = kfun.kernelMatrix(param.KparDiff, z)
            ra = kernel.applyK(z, a)
            if hasattr(self, 'v'):  
                self.v[t, :] = ra
            obj = obj + regWeight*timeStep*np.multiply(a, (ra)).sum()

            if self.affineDim > 0:
                obj1 +=  timeStep * np.multiply(self.affineWeight.reshape(Afft[t].shape), Afft[t]**2).sum()
            #print xt.sum(), at.sum(), obj
        #print obj, obj+obj1
        obj += obj1
        if self.verbose:
            print('objectiveFunDef obj =', obj)
        if withJacobian:
            return obj, xt, Jt
        elif withTrajectory:
            return obj, xt
        else:
            return obj


    def objectiveFun(self):
        if self.verbose:
            print('Calculate obj fun E(a,theta)')
        if self.obj == None: #?
            (self.obj, self.xt) = self.objectiveFunDef(self.at, self.Afft, withTrajectory=True)
            #foo = surfaces.Surface(surf=self.fvDef)
            self.fvDef = np.copy(np.squeeze(self.xt[-1, :, :]))
            #foo.computeCentersAreas()
            self.obj += self.dataTerm(self.fvDef)
            #print self.obj0,  self.dataTerm(self.fvDef)
        else:
            print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        return self.obj

    def getVariable(self):
        return [self.at, self.Afft, self.x0]

    def updateTry(self, dir, eps, objRef=None):
        #print('atTry')
        objTry = 0
        atTry = self.at - eps * dir.diff
        if self.affineDim > 0:
            AfftTry = self.Afft - eps * dir.aff
        else:
            AfftTry = self.Afft
        x0Try = self.x0

        if self.verbose:
            print('Calculate obj fun E(a,theta) - updateTry')
        foo = self.objectiveFunDef(atTry, AfftTry, x0 = x0Try, withTrajectory=True)
        objTry += foo[0]
        ff = np.copy(np.squeeze(foo[1][-1, :, :]))
        objTry += self.dataTerm(ff)

        if np.isnan(objTry):
            logging.info('Warning: nan in updateTry')
            return 1e500

        if (objRef == None) | (objTry < objRef):
            self.atTry = atTry
            self.objTry = objTry
            self.AfftTry = AfftTry
            self.x0Try = x0Try
            #print 'objTry=',objTry, dir.diff.sum()

        return objTry


    def testEndpointGradient(self):
        if self.verbose:
            print('testEndpointGradient')
        c0 = self.dataTerm(self.fvDef)
        ff = np.copy(self.fvDef)
        dff = np.random.normal(size=ff.shape)
        eps = 1e-6
        ff += eps*dff
        c1 = self.dataTerm(ff)
        grd = self.endPointGradient()
        logging.info("test endpoint gradient: {0:.5f} {1:.5f}".format((c1-c0)/eps, (grd*dff).sum()) )

    def endPointGradient(self):
        if self.verbose:
            print('Calculate px1')
        if self.param.errorType == 'classification':
            px = pointSets.LogisticScoreL2Gradient(self.fvDef, self.fv1, self.u, w=self.wTr, intercept=self.intercept, l1Cost=self.l1Cost) \
                 / self.param.sigmaError**2 #?
        elif self.param.errorType == 'regression':
            px = pointSets.RegressionScoreGradient(self.fvDef, self.fv1, self.u) / self.param.sigmaError**2 #?
        return px

    
    def hamiltonianCovector(self, x0, at, px1, KparDiff, regWeight, affine = None):
        N = x0.shape[0]
        dim = x0.shape[1]
        M = at.shape[0]
        timeStep = 1.0/M
        xt = evol.landmarkDirectEvolutionEuler(x0, at, KparDiff, affine=affine)
        if not(affine is None):
            A0 = affine[0]
            A = np.zeros([M,dim,dim])
            for k in range(A0.shape[0]):
                A[k,...] = getExponential(timeStep*A0[k]) 
        else:
            A = np.zeros([M,dim,dim])
            for k in range(M):
                A[k,...] = np.eye(dim) 
                
        pxt = np.zeros([M+1, N, dim])
        pxt[M, :, :] = px1
        foo = np.copy(self.fv0)
        for t in range(M):
            px = np.squeeze(pxt[M-t, :, :])
            z = np.squeeze(xt[M-t-1, :, :])
            a = np.squeeze(at[M-t-1, :, :])
            foo = np.copy(z)
            v = KparDiff.applyK(z,a)
            a1 = np.concatenate((px[np.newaxis,...], a[np.newaxis,...], -2*regWeight*a[np.newaxis,...]))
            a2 = np.concatenate((a[np.newaxis,...], px[np.newaxis,...], a[np.newaxis,...]))
            zpx = KparDiff.applyDiffKT(z, a1, a2)
                
            if not (affine is None):
                pxt[M-t-1, :, :] = np.dot(px, A[M-t-1]) + timeStep * zpx
            else:
                pxt[M-t-1, :, :] = px + timeStep * zpx
        return pxt, xt
    
    def hamiltonianGradient(self, px1, kernel = None, affine=None, regWeight=None, x0=None, at=None):
        if self.verbose:
            print('Calculate pxt')
        #print('Calculate dat, dA, db, xt, pxt')
        if regWeight is None:
            regWeight = self.regweight
        if x0 is None:
            x0 = self.x0
        if at is None:
            at = self.at
        if kernel is None:
            kernel  = self.param.KparDiff
        return evol.landmarkHamiltonianGradient(x0, at, px1, kernel, regWeight, affine=affine,
                                                getCovector=True)

    def getGradient(self, coeff=1.0):
        px1 = -self.endPointGradient()
        A = [np.zeros([self.Tsize, self.dim, self.dim]), np.zeros([self.Tsize, self.dim])]
        dim2 = self.dim**2
        if self.affineDim > 0:
            for t in range(self.Tsize):
                AB = np.dot(self.affineBasis, self.Afft[t])
                A[0][t] = AB[0:dim2].reshape([self.dim, self.dim])
                A[1][t] = AB[dim2:dim2+self.dim]
        foo = self.hamiltonianGradient(px1, affine=A)
        grd = Direction()
        grd.diff = foo[0]/(coeff*self.Tsize)
        grd.aff = np.zeros(self.Afft.shape)
        if self.affineDim > 0:
            dA = foo[1]
            db = foo[2]
            grd.aff = 2*self.affineWeight.reshape([1, self.affineDim])*self.Afft
            #grd.aff = 2 * self.Afft
            for t in range(self.Tsize):
               dAff = np.dot(self.affineBasis.T, np.vstack([dA[t].reshape([dim2,1]), db[t].reshape([self.dim, 1])]))
               #grd.aff[t] -=  np.divide(dAff.reshape(grd.aff[t].shape), self.affineWeight.reshape(grd.aff[t].shape))
               grd.aff[t] -=  dAff.reshape(grd.aff[t].shape)
            grd.aff /= (self.coeffAff*coeff*self.Tsize)
            #            dAfft[:,0:self.dim**2]/=100
        return grd


    def addProd(self, dir1, dir2, beta):
        dir = Direction()
        dir.diff = dir1.diff + beta * dir2.diff
        dir.aff = dir1.aff + beta * dir2.aff
        return dir

    def prod(self, dir1, beta):
        dir = Direction()
        dir.diff = beta * dir1.diff
        dir.aff = beta * dir1.aff
        return dir

    def copyDir(self, dir0):
        dir = Direction()
        dir.diff = np.copy(dir0.diff)
        dir.aff = np.copy(dir0.aff)
        return dir

    def randomDir(self):
        dirfoo = Direction()
        dirfoo.diff = np.random.randn(self.Tsize, self.npt, self.dim)
        dirfoo.aff = np.random.randn(self.Tsize, self.affineDim)
        return dirfoo

    def dotProduct(self, g1, g2):
        res = np.zeros(len(g2))
        for t in range(self.Tsize):
            z = np.squeeze(self.xt[t, :, :])
            gg = np.squeeze(g1.diff[t, :, :])
            u = self.param.KparDiff.applyK(z, gg)
            uu = g1.aff[t]
            ll = 0
            for gr in g2:
                ggOld = np.squeeze(gr.diff[t, :, :])
                res[ll]  = res[ll] + np.multiply(ggOld,u).sum()
                if self.affineDim > 0:
                    res[ll] += np.multiply(uu, gr.aff[t]).sum() * self.coeffAff
                ll = ll + 1
        return res

    def acceptVarTry(self):
        self.obj = self.objTry
        self.at = np.copy(self.atTry)
        self.Afft = np.copy(self.AfftTry)
        self.x0 = np.copy(self.x0Try)
        #print self.at

    def endOfIteration(self, endP=False):
        if self.verbose:
            print('endOfIteration')
        self.iter += 1
        if self.testGradient:
            self.testEndpointGradient()

        if self.iter >= self.affBurnIn:
            self.coeffAff = self.coeffAff2
        if (self.iter % self.saveRate == 0 or endP) :
            logging.info('Saving Points...')
            (obj1, self.xt) = self.objectiveFunDef(self.at, self.Afft, withTrajectory=True)

            self.fvDef = np.copy(np.squeeze(self.xt[-1, :, :]))
            dim2 = self.dim**2
            A = [np.zeros([self.Tsize, self.dim, self.dim]), np.zeros([self.Tsize, self.dim])]
            if self.affineDim > 0:
                for t in range(self.Tsize):
                    AB = np.dot(self.affineBasis, self.Afft[t])
                    A[0][t] = AB[0:dim2].reshape([self.dim, self.dim])
                    A[1][t] = AB[dim2:dim2+self.dim]
            (xt, Jt)  = evol.landmarkDirectEvolutionEuler(self.x0, self.at, self.param.KparDiff, affine=A,
                                                              withJacobian=True)
            if self.affine=='euclidean' or self.affine=='translation':
                f = np.copy(self.fv0)
                X = self.affB.integrateFlow(self.Afft)
                displ = np.zeros(self.x0.shape[0])
                dt = 1.0 /self.Tsize
                for t in range(self.Tsize+1):
                    U = la.inv(X[0][t])
                    yyt = np.dot(self.xt[t,...] - X[1][t, ...], U.T)
                    f = np.copy(yyt)
                    pointSets.savelmk(f, self.outputDir + '/' + self.saveFile + 'Corrected' + str(t) + '.lmk')
                f = np.copy(self.fv1)
                yyt = np.dot(f - X[1][-1, ...], U.T)
                f = np.copy(yyt)
                pointSets.savePoints(self.outputDir + '/TargetCorrected.vtk', f)
            for kk in range(self.Tsize+1):
                fvDef = np.copy(np.squeeze(xt[kk, :, :]))
                pointSets.savePoints(self.outputDir + '/' + self.saveFile + str(kk) + '.vtk', fvDef)

            if self.param.errorType == 'classification':
                if self.intercept:
                    xDef1 = np.concatenate((np.ones((self.fvDef.shape[0], 1)), self.fvDef), axis=1)
                else:
                    xDef1 = self.fvDef
                gu = np.argmax(np.dot(xDef1, self.u), axis=1)[:,np.newaxis]
                self.guTr = np.copy(gu)
                train_err = np.sum(np.not_equal(gu, self.fv1) * self.wTr)/self.swTr
                logging.info('Training Error {0:0f}'.format(train_err))
                if train_err > 0.05:
                    self.param.sigmaError *= 1 - min(train_err, 0.05)
                    logging.info('Reducing sigma:  {0:f}'.format(self.param.sigmaError))
                    self.reset = True
                if self.nclasses < 3:
                    pcau = PCA(n_components=self.nclasses) #?
                else:
                    pcau = PCA(n_components=3)

                if self.intercept:
                    ofs = 1
                    b = 0
                else:
                    ofs = 0
                    b = 0

                U,S,V = la.svd(self.u[ofs:self.dim+ofs,:])

                xRes = np.dot(self.fvDef, U)
                if self.nclasses < 4:
                    xTr3 = xRes[:, 0:self.nclasses-1] - b
                    xRes = xRes[:,self.nclasses-1:self.dim]
                    if xRes.shape[1] > 4-self.nclasses:
                        pca = PCA(4-self.nclasses)
                        xRes = pca.fit_transform(xRes)
                    xTr3 = np.concatenate((xTr3, xRes), axis=1)
                else:
                    xTr3 = xRes[:, 0:3] - b[0:3]
                self.xTr3 = xTr3
                if self.testSet is not None:
                    testRes = evol.landmarkDirectEvolutionEuler(self.x0, self.at, self.param.KparDiff, withPointSet=self.testSet[0])
                    x0Tr = self.fvDef
                    x0Te = testRes[1][testRes[1].shape[0]-1,...]
                    x1Tr = self.fv1
                    x1Te = self.testSet[1]
                    self.testDef = x0Te
                    if self.intercept:
                        xDef1 = np.concatenate((np.ones((self.testSet[0].shape[0], 1)), x0Te), axis=1)
                    else:
                        xDef1 = x0Te
                    gu = np.argmax(np.dot(xDef1, self.u), axis=1)[:,np.newaxis]
                    self.guTe = np.copy(gu)
                    test_err = np.sum(np.not_equal(gu, self.testSet[1])*self.wTe)/self.swTe
                    self.testError.logistic = test_err
                    logging.info('Testing Error {0:0f}'.format(test_err))
                    xRes = np.dot(x0Te, U)
                    if self.nclasses < 4:
                        xTe3 = xRes[:, 0:self.nclasses - 1] - b
                        if xRes.shape[1] > 3:
                            xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                        else:
                            xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                        xTe3 = np.concatenate((xTe3, xRes), axis=1)
                    else:
                        xTe3 = xRes[:, 0:3] - b[0:3]

                    self.testError = trainTest.classTrainTest(self.testError, self, x0Tr, x1Tr, x0Te, x1Te, 1)
                    self.xTe3 = xTe3

                if self.pplot:
                    plotClassRegress(self, 0)

                if self.intercept:
                    ofs = 1
                else:
                    ofs = 0

                for kk in range(self.Tsize + 1):
                    fvDef = np.copy(np.squeeze(xt[kk, :, :]))
                    xRes = np.dot(fvDef, U)
                    if self.nclasses < 4:
                        x3 = xRes[:, 0:self.nclasses - 1]
                        if xRes.shape[1] > 4:
                            xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                        else:
                            xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                        x3 = np.concatenate((x3, xRes), axis=1)
                    else:
                        x3 = xRes[:, 0:3]
                    pointSets.savePoints(self.outputDir + '/' + self.saveFile + str(kk) + '.vtk', x3,
                                         scalars=np.ravel(self.fv1))
                if self.testSet is not None:
                    for kk in range(self.Tsize + 1):
                        fvDef = np.copy(np.squeeze(testRes[1][kk, :, :]))
                        xRes = np.dot(fvDef, U)
                        if self.nclasses < 4:
                            x3 = xRes[:, 0:self.nclasses - 1]
                            if xRes.shape[1] > 4:
                                xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                            else:
                                xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                            x3 = np.concatenate((x3, xRes), axis=1)
                        else:
                            x3 = xRes[:, 0:3]
                        pointSets.savePoints(self.outputDir + '/' + self.saveFile + 'Test' + str(kk) + '.vtk', x3,
                                             scalars=np.ravel(self.testSet[1]))

            elif self.param.errorType == 'regression':
                xDef1 = np.concatenate((np.ones((self.fvDef.shape[0], 1)), self.fvDef), axis=1)
                if self.verbose:
                    print('xDef1.shape =', xDef1.shape)
                    print('self.u.shape =', self.u.shape)
                gu = np.dot(xDef1, self.u)
                self.guTr = np.copy(gu)
                train_err = ((gu - self.fv1) ** 2).sum() / self.fv1.shape[0]
                logging.info('Training Error {0:0f}'.format(train_err))
                if train_err > 0.05:
                     self.param.sigmaError *= 1 - min(train_err, 0.05)
                     logging.info('Reducing sigma:  {0:f}'.format(self.param.sigmaError))
                     self.reset = True


                # if self.nclasses < 3:
                #     pcau = PCA(n_components=self.nclasses)
                # else:
                #      pcau = PCA(n_components=3)

                # if self.intercept:
                #     ofs = 1
                #     b = 0
                # else:
                #      ofs = 0
                #      b = 0
                ofs = 1
                b = 0

                # U,S,V = la.svd(self.u[ofs:self.dim+ofs,:])
                U,S,V = la.svd(self.u[ofs:self.dim+ofs,:])

                xRes = np.dot(self.fvDef, U)
                xTr3 = xRes[:, 0:1] - b
                self.xTr3 = xTr3
                xRes = xRes[:,1:self.dim]
                pca = PCA(4-2)
                xRes = pca.fit_transform(xRes)
                xTr3 = np.concatenate((xTr3, xRes), axis=1)
                self.xTr3 = xTr3

                # xTr3 = x0Tr[:, 0:3]

                # xRes = np.dot(self.fvDef, U)
                # if self.nclasses < 4:
                #     xTr3 = xRes[:, 0:self.nclasses-1] - b
                #     xRes = xRes[:,self.nclasses-1:self.dim]
                #     if xRes.shape[1] > 4-self.nclasses:
                #         pca = PCA(4-self.nclasses)
                #         xRes = pca.fit_transform(xRes)
                #     xTr3 = np.concatenate((xTr3, xRes), axis=1)
                # else:
                #     xTr3 = xRes[:, 0:3] - b[0:3]

                if self.testSet is not None:
                    testRes = evol.landmarkDirectEvolutionEuler(self.x0, self.at, self.param.KparDiff, withPointSet=self.testSet[0])
                    x0Tr = self.fvDef
                    x0Te = testRes[1][testRes[1].shape[0]-1,...]
                    x1Tr = self.fv1
                    x1Te = self.testSet[1]
                    self.testDef = x0Te
                    xDef1 = np.concatenate((np.ones((self.testSet[0].shape[0], 1)), x0Te), axis=1)
                    gu = np.dot(xDef1, self.u)
                    self.guTe = np.copy(gu)
                    test_err = ((gu - self.testSet[1]) ** 2).sum() / self.testSet[1].shape[0]
                    self.testError.leastSq = test_err
                    logging.info('Testing Error {0:0f}'.format(test_err))

                    xRes = np.dot(x0Te, U)
                    xTe3 = xRes[:, 0:1] - b
                    if xRes.shape[1] > 3:
                        xRes = pca.transform(xRes[:, 1:xRes.shape[1]])
                    else:
                        xRes = xRes[:,1:xRes.shape[1]]
                    xTe3 = np.concatenate((xTe3, xRes), axis=1)
                    # xTe3 = x0Te[:, 0:3]

                    # xRes = np.dot(x0Te, U)
                    # if self.nclasses < 4:
                    #     xTe3 = xRes[:, 0:self.nclasses - 1] - b
                    #     if xRes.shape[1] > 3:
                    #         xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                    #     else:
                    #         xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                    #     xTe3 = np.concatenate((xTe3, xRes), axis=1)
                    # else:
                    #     xTe3 = xRes[:, 0:3] - b[0:3]

                    self.testError = trainTest.regressTrainTest(self.testError, self, x0Tr, x1Tr, x0Te, x1Te, 1)
                    self.xTe3 = xTe3

                if self.pplot:
                    plotClassRegress(self, 0)

                # if self.intercept:
                #     ofs = 1
                # else:
                #     ofs = 0
                #
                # for kk in range(self.Tsize + 1):
                #     fvDef = np.copy(np.squeeze(xt[kk, :, :]))
                #     xRes = np.dot(fvDef, U)
                #     if self.nclasses < 4:
                #         x3 = xRes[:, 0:self.nclasses - 1]
                #         if xRes.shape[1] > 4:
                #             xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                #         else:
                #             xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                #         x3 = np.concatenate((x3, xRes), axis=1)
                #     else:
                #         x3 = xRes[:, 0:3]
                #     pointSets.savePoints(self.outputDir + '/' + self.saveFile + str(kk) + '.vtk', x3,
                #                          scalars=np.ravel(self.fv1))
                # if self.testSet is not None:
                #     for kk in range(self.Tsize + 1):
                #         fvDef = np.copy(np.squeeze(testRes[1][kk, :, :]))
                #         xRes = np.dot(fvDef, U)
                #         if self.nclasses < 4:
                #             x3 = xRes[:, 0:self.nclasses - 1]
                #             if xRes.shape[1] > 4:
                #                 xRes = pca.transform(xRes[:, self.nclasses-1:xRes.shape[1]])
                #             else:
                #                 xRes = xRes[:, self.nclasses-1:xRes.shape[1]]
                #             x3 = np.concatenate((x3, xRes), axis=1)
                #         else:
                #             x3 = xRes[:, 0:3]
                #         pointSets.savePoints(self.outputDir + '/' + self.saveFile + 'Test' + str(kk) + '.vtk', x3,
                #                              scalars=np.ravel(self.testSet[1]))

        else:
            (obj1, self.xt) = self.objectiveFunDef(self.at, self.Afft, withTrajectory=True)
            self.fvDef = np.copy(np.squeeze(self.xt[-1, :, :]))
        if self.param.errorType == 'classification' and self.relearnRate > 0 and (self.iter % self.relearnRate == 0):
            u0 = self.u
            self.rnd = 1- self.coeffrnd*(1-self.rnd)
            if self.verbose:
                print('test learnLogistic')
            self.u = self.learnLogistic(u0=self.u, random=self.rnd)
            #print('u0 = ', u0)
            #print('self.u = ', self.u)
            logging.info('Resetting weights: delta u = {0:f}, norm u = {1:f} '.format(np.sqrt(((self.u - u0) ** 2).sum()),
                                                                                      np.sqrt(((self.u) ** 2).sum())))
            self.reset = True
        elif self.param.errorType == 'regression' and self.relearnRate > 0 and (self.iter % self.relearnRate == 0):
            u0 = self.u
            self.rnd = 1- self.coeffrnd*(1-self.rnd)
            if self.verbose:
                print('test learnRegression')
            self.u = self.learnRegression()
            #print('u0 = ', u0)
            #print('self.u = ', self.u)
            logging.info('Resetting weights: delta u = {0:f}, norm u = {1:f} '.format(np.sqrt(((self.u - u0) ** 2).sum()),
                                                                                      np.sqrt(((self.u) ** 2).sum())))
            self.reset = True


    def endOfProcedure(self):
        if self.verbose:
            print('endOfProcedure')
        self.endOfIteration(endP=True)

    def optimizeMatching(self):
        self.coeffAff = self.coeffAff2
        if self.verbose:
            print('optimizeMatching')
        grd = self.getGradient(self.gradCoeff)
        [grd2] = self.dotProduct(grd, [grd])

        self.gradEps = max(0.001, np.sqrt(grd2) / 10000)
        logging.info('Gradient lower bound: %f' %(self.gradEps))
        self.coeffAff = self.coeffAff1
        #self.restartRate = self.relearnRate
        cg.cg(self, verb = self.verb, maxIter = self.maxIter,TestGradient=self.testGradient, epsInit=0.1, verbose=self.verbose)
        # bfgs.bfgs(self, verb = self.verb, maxIter = self.maxIter,TestGradient=self.testGradient, epsInit=0.1)
        #return self.at, self.xt

    def learnLogistic(self, u0=None, random = 1.0):
        return pointSets.learnLogisticL2(self.fvDef, self.fv1, w= self.wTr, u0=u0, l1Cost=self.l1Cost,
                                         intercept=self.intercept, random=random, verbose=self.verbose)

    def learnRegression(self):
        return pointSets.learnRegression(self.fvDef, self.fv1, self.verbose)

    def localMaps1D(self, d):
        KL1 = np.arange(0, d, dtype=int)
        KL0 = np.zeros(4*d-2, dtype=int)
        ii = 0
        for i in range(d):
            if i>0:
                KL0[ii] = i-1
                ii += 1
            KL0[ii] = i
            ii += 1
            if i<d-1:
                KL0[ii] = i+1
                ii += 1
            KL0[ii] = -1
            if i < d-1:
                ii += 1
        return (KL0, KL1)

    def localMapsCircle(self, d):
        KL1 = np.arange(0, d, dtype=int)
        KL0 = np.zeros(4*d, dtype=int)
        ii = 0
        for i in range(d):
            KL0[ii] = (i-1)%d
            ii += 1
            KL0[ii] = i
            ii += 1
            KL0[ii] = (i+1)%d
            ii += 1
            KL0[ii] = -1
            if i < d-1:
                ii += 1
        return (KL0, KL1)

    def localMapsNaive(self, d):
        KL1 = np.arange(0, d, dtype=int)
        KL0 = np.zeros(2*d, dtype=int)
        ii = 0
        for i in range(d):
            KL0[ii] = i
            ii += 1
            KL0[ii] = -1
            if i < d-1:
                ii += 1

        return (KL0, KL1)

    def localMapsPredict(self, d, i0):
        KL1 = np.zeros(d, dtype=int)
        KL0 = np.zeros(2*d + 1, dtype=int)
        ii = 0
        for i in range(d):
            if i != i0:
                KL0[ii] = i
                ii += 1
            else:
                KL1[i] = 1
        KL0[ii] = -1
        ii += 1
        for i in range(d):
            KL0[ii] = i
            ii += 1
        KL0[ii] = -1
