import os
import numpy as np
import matplotlib
matplotlib.use("TKAgg")
import matplotlib.pyplot as plt
import matplotlib.colors
#import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import warnings
import matplotlib.cbook
warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)


def plotClassRegress(f, flag_init):

    vmin = -1.2
    vmax = 1.2

    if flag_init == 1:

        if f.param.errorType == 'classification':

            colors = ['red', 'blue', 'coral', 'green', 'gold', 'maroon', 'magenta', 'olive', 'lime', 'purple']
            fig = plt.figure(1)
            fig.clf()
            if f.testSet is not None:
                fig.set_size_inches(10 * 2, 8)
                if f.plotDim == 2:
                    ax1 = fig.add_subplot(241)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                    plt.title('Original Training Dataset (Truth)')
                    ax2 = fig.add_subplot(245)
                    ax2.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], marker='*', c=np.array(colors)[f.testSet[1][:, 0]])
                    plt.title('Original Test Dataset (Truth)')
                    ax3 = fig.add_subplot(242)
                    ax3.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                    plt.title('Original Training Dataset (Predict)')
                    ax4 = fig.add_subplot(246)
                    ax4.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                    plt.title('Original Test Dataset (Predict)')
                elif f.plotDim == 3:
                    ax1 = fig.add_subplot(241, projection='3d')
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                    plt.title('Original Training Dataset (Truth)')
                    ax2 = fig.add_subplot(245, projection='3d')
                    ax2.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], f.testSet[0][:, 2], marker='*', c=np.array(colors)[f.testSet[1][:, 0]])
                    plt.title('Original Test Dataset (Truth)')
                    ax3 = fig.add_subplot(242, projection='3d')
                    ax3.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                    plt.title('Original Training Dataset (Predict)')
                    ax4 = fig.add_subplot(246, projection='3d')
                    ax4.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], f.testSet[0][:, 2], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                    plt.title('Original Test Dataset (Predict)')
            else:
                fig.set_size_inches(10 * 2, 8 / 2)
                if f.plotDim == 2:
                    ax1 = fig.add_subplot(141)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                    plt.title('Original Training Dataset (Truth)')
                    ax2 = fig.add_subplot(142)
                    ax2.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                    plt.title('Original Training Dataset (Predict)')
                elif f.plotDim == 3:
                    ax1 = fig.add_subplot(141, projection='3d')
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                    plt.title('Original Training Dataset (Truth)')
                    ax2 = fig.add_subplot(142, projection='3d')
                    ax2.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                    plt.title('Original Training Dataset (Predict)')
            plt.tight_layout()
            plt.pause(0.1)

        elif f.param.errorType == 'regression':

            cmap = plt.cm.rainbow
            norm = matplotlib.colors.Normalize(vmin, vmax)
            sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
            sm.set_array([])

            fig = plt.figure(1)
            fig.clf()
            if f.testSet is not None:
                fig.set_size_inches(10*2, 8)
                if f.plotDim == 2:
                    ax1 = fig.add_subplot(241)
                    c = np.ravel(f.fv1)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Truth)')
                    #fig.colorbar(sm)
                    ax2 = fig.add_subplot(245)
                    c = np.ravel(f.testSet[1])
                    ax2.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], marker='*', c=cmap(norm(c)))
                    plt.title('Original Test Dataset (Truth)')
                    #fig.colorbar(sm)
                    ax3 = fig.add_subplot(242)
                    c = np.ravel(f.guTr)
                    ax3.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Predict)')
                    # fig.colorbar(sm)
                    ax4 = fig.add_subplot(246)
                    c = np.ravel(f.guTe)
                    ax4.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], marker='*', c=cmap(norm(c)))
                    plt.title('Original Test Dataset (Predict)')
                    # fig.colorbar(sm)
                elif f.plotDim == 3:
                    ax1 = fig.add_subplot(241, projection='3d')
                    c = np.ravel(f.fv1)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Truth)')
                    #fig.colorbar(sm)
                    ax2 = fig.add_subplot(245, projection='3d')
                    c = np.ravel(f.testSet[1])
                    ax2.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], f.testSet[0][:, 2], marker='*', c=cmap(norm(c)))
                    plt.title('Original Test Dataset (Truth)')
                    #fig.colorbar(sm)
                    ax3 = fig.add_subplot(242, projection='3d')
                    c = np.ravel(f.guTr)
                    ax3.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Predict)')
                    # fig.colorbar(sm)
                    ax4 = fig.add_subplot(246, projection='3d')
                    c = np.ravel(f.guTe)
                    ax4.scatter(f.testSet[0][:, 0], f.testSet[0][:, 1], f.testSet[0][:, 2], marker='*', c=cmap(norm(c)))
                    plt.title('Original Test Dataset (Predict)')
                    # fig.colorbar(sm)
            else:
                fig.set_size_inches(10*2, 8/2)
                if f.plotDim == 2:
                    ax1 = fig.add_subplot(141)
                    c = np.ravel(f.fv1)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Truth)')
                    # fig.colorbar(sm)
                    ax2 = fig.add_subplot(142)
                    c = np.ravel(f.guTr)
                    ax2.scatter(f.fvDef[:, 0], f.fvDef[:, 1], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Predict)')
                    # fig.colorbar(sm)
                elif f.plotDim == 3:
                    ax1 = fig.add_subplot(141, projection='3d')
                    c = np.ravel(f.fv1)
                    ax1.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Truth)')
                    # fig.colorbar(sm)
                    ax2 = fig.add_subplot(142, projection='3d')
                    c = np.ravel(f.guTr)
                    ax2.scatter(f.fvDef[:, 0], f.fvDef[:, 1], f.fvDef[:, 2], marker='o', c=cmap(norm(c)))
                    plt.title('Original Training Dataset (Predict)')
                    # fig.colorbar(sm)
            plt.tight_layout()
            plt.pause(0.1)

    elif flag_init == 0:

        fig = plt.gcf()
        allAxes = fig.get_axes()
        nAxes = len(allAxes)

        if f.param.errorType == 'classification':

            colors = ['red', 'blue', 'coral', 'green', 'gold', 'maroon', 'magenta', 'olive', 'lime', 'purple']
            fig = plt.gcf()
            if f.testSet is not None:
                if f.plotDim == 2:
                    if nAxes == 4:
                        ax3 = fig.add_subplot(243)
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        plt.title('Transformed Training Dataset (Truth)')
                        ax4 = fig.add_subplot(247)
                        ax4.clear()
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=np.array(colors)[f.testSet[1][:, 0]])
                        plt.title('Transformed Test Dataset (Truth)')
                        ax5 = fig.add_subplot(244)
                        ax5.clear()
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        plt.title('Transformed Training Dataset (Predict)')
                        ax6 = fig.add_subplot(248)
                        ax6.clear()
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                        plt.title('Transformed Test Dataset (Predict)')
                    else:
                        ax3 = allAxes[4]
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        ax4 = allAxes[5]
                        ax4.clear()
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=np.array(colors)[f.testSet[1][:, 0]])
                        ax4.set_title('Transformed Test Dataset (Truth)')
                        ax5 = allAxes[6]
                        ax5.clear()
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        ax5.set_title('Transformed Training Dataset (Predict)')
                        ax6 = allAxes[7]
                        ax6.clear()
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                        ax6.set_title('Transformed Test Dataset (Predict)')
                elif f.plotDim == 3:
                    if nAxes == 4:
                        ax3 = fig.add_subplot(243, projection='3d')
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        plt.title('Transformed Training Dataset (Truth)')
                        ax4 = fig.add_subplot(247, projection='3d')
                        ax4.clear()
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*',
                                    c=np.array(colors)[f.testSet[1][:, 0]])
                        plt.title('Transformed Test Dataset (Truth)')
                        ax5 = fig.add_subplot(244, projection='3d')
                        ax5.clear()
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        plt.title('Transformed Training Dataset (Predict)')
                        ax6 = fig.add_subplot(248, projection='3d')
                        ax6.clear()
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                        plt.title('Transformed Test Dataset (Predict)')
                    else:
                        ax3 = allAxes[4]
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        ax4 = allAxes[5]
                        ax4.clear()
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c = np.array(colors)[f.testSet[1][:, 0]])
                        ax4.set_title('Transformed Test Dataset (Truth)')
                        ax5 = allAxes[6]
                        ax5.clear()
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        ax5.set_title('Transformed Training Dataset (Predict)')
                        ax6 = allAxes[7]
                        ax6.clear()
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=np.array(colors)[f.guTe[:, 0]])
                        ax6.set_title('Transformed Test Dataset (Predict)')
            else:
                if f.plotDim == 2:
                    if nAxes == 2:
                        ax3 = fig.add_subplot(143)
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        plt.title('Transformed Training Dataset (Truth)')
                        ax4 = fig.add_subplot(144)
                        ax4.clear()
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        plt.title('Transformed Training Dataset (Predict)')
                    else:
                        ax3 = allAxes[2]
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        ax4 = allAxes[3]
                        ax4.clear()
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        ax4.set_title('Transformed Training Dataset (Predict)')
                elif f.plotDim == 3:
                    if nAxes == 2:
                        ax3 = fig.add_subplot(143, projection='3d')
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.fv1[:, 0]])
                        plt.title('Transformed Training Dataset (Truth)')
                        ax4 = fig.add_subplot(144, projection='3d')
                        ax4.clear()
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=np.array(colors)[f.guTr[:, 0]])
                        plt.title('Transformed Training Dataset (Predict)')
                    else:
                        ax3 = allAxes[2]
                        ax3.clear()
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o',
                                    c=np.array(colors)[f.fv1[:, 0]])
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        ax4 = allAxes[3]
                        ax4.clear()
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o',
                                    c=np.array(colors)[f.guTr[:, 0]])
                        ax4.set_title('Transformed Training Dataset (Predict)')
            plt.tight_layout()
            plt.pause(0.1)

        elif f.param.errorType == 'regression':

            cmap = plt.cm.rainbow
            norm = matplotlib.colors.Normalize(vmin, vmax)
            sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
            sm.set_array([])

            if f.testSet is not None:
                if f.plotDim == 2:
                    if nAxes == 4:
                        ax3 = fig.add_subplot(243)
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = fig.add_subplot(247)
                        ax4.clear()
                        c = np.ravel(f.testSet[1])
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=cmap(norm(c)))
                        plt.title('Transformed Test Dataset (Truth)')
                        ax5 = fig.add_subplot(244)
                        ax5.clear()
                        c = np.ravel(f.guTr)  # np.ravel(x1Tr)
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                        ax6 = fig.add_subplot(248)
                        ax6.clear()
                        c = np.ravel(f.guTe)  # np.ravel(x1Te)
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=cmap(norm(c)))
                        plt.title('Transformed Test Dataset (Predict)')
                        # fig.colorbar(sm)
                    else:
                        ax3 = allAxes[4]
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = allAxes[5]
                        ax4.clear()
                        c = np.ravel(f.testSet[1])
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=cmap(norm(c)))
                        ax4.set_title('Transformed Test Dataset (Truth)')
                        ax5 = allAxes[6]
                        ax5.clear()
                        c = np.ravel(f.guTr)  # np.ravel(x1Tr)
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        ax5.set_title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                        ax6 = allAxes[7]
                        ax6.clear()
                        c = np.ravel(f.guTe)  # np.ravel(x1Te)
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], marker='*', c=cmap(norm(c)))
                        ax6.set_title('Transformed Test Dataset (Predict)')
                        # fig.colorbar(sm)
                elif f.plotDim == 3:
                    if nAxes == 4:
                        ax3 = fig.add_subplot(243, projection='3d')
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = fig.add_subplot(247, projection='3d')
                        ax4.clear()
                        c = np.ravel(f.testSet[1])
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=cmap(norm(c)))
                        plt.title('Transformed Test Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax5 = fig.add_subplot(244, projection='3d')
                        ax5.clear()
                        c = np.ravel(f.guTr)  # np.ravel(x1Tr)
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                        ax6 = fig.add_subplot(248, projection='3d')
                        ax6.clear()
                        c = np.ravel(f.guTe)  # np.ravel(x1Te)
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=cmap(norm(c)))
                        plt.title('Transformed Test Dataset (Predict)')
                        # fig.colorbar(sm)
                    else:
                        ax3 = allAxes[4]
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = allAxes[5]
                        ax4.clear()
                        c = np.ravel(f.testSet[1])
                        ax4.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=cmap(norm(c)))
                        ax4.set_title('Transformed Test Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax5 = allAxes[6]
                        ax5.clear()
                        c = np.ravel(f.guTr)  # np.ravel(x1Tr)
                        ax5.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        ax5.set_title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                        ax6 = allAxes[7]
                        ax6.clear()
                        c = np.ravel(f.guTe)  # np.ravel(x1Te)
                        ax6.scatter(f.xTe3[:, 0], f.xTe3[:, 1], f.xTe3[:, 2], marker='*', c=cmap(norm(c)))
                        ax6.set_title('Transformed Test Dataset (Predict)')
                        # fig.colorbar(sm)
                print('guTr.min(), guTr.max() =', f.guTr.min(), f.guTr.max())
                print('guTe.min(), guTe.max() =', f.guTe.min(), f.guTe.max())
            else:
                if f.plotDim == 2:
                    if nAxes == 2:
                        ax3 = fig.add_subplot(143)
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = fig.add_subplot(144)
                        ax4.clear()
                        c = np.ravel(f.guTr)
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                    else:
                        ax3 = allAxes[2]
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = allAxes[3]
                        ax4.clear()
                        c = np.ravel(f.guTr)
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], marker='o', c=cmap(norm(c)))
                        ax4.set_title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                elif f.plotDim == 3:
                    if nAxes == 2:
                        ax3 = fig.add_subplot(143, projection='3d')
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = fig.add_subplot(144, projection='3d')
                        ax4.clear()
                        c = np.ravel(f.guTr)
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        plt.title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
                    else:
                        ax3 = allAxes[2]
                        ax3.clear()
                        c = np.ravel(f.fv1)
                        ax3.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        ax3.set_title('Transformed Training Dataset (Truth)')
                        # fig.colorbar(sm)
                        ax4 = allAxes[3]
                        ax4.clear()
                        c = np.ravel(f.guTr)
                        ax4.scatter(f.xTr3[:, 0], f.xTr3[:, 1], f.xTr3[:, 2], marker='o', c=cmap(norm(c)))
                        ax4.set_title('Transformed Training Dataset (Predict)')
                        # fig.colorbar(sm)
            plt.tight_layout()
            plt.pause(0.1)
