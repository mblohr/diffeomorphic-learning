import numpy as np
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
import logging

def regressTrainTest(testError, f, x0Tr, x1Tr, x0Te, x1Te, flag):

    if flag == 0:

        clf = svm.SVR(gamma='scale')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.SVM = ((yTe - np.ravel(x1Te))**2).sum() / x1Te.shape[0]
        print('SVM prediction:', ((yTr - np.ravel(x1Tr))**2).sum() / x1Tr.shape[0], testError.SVM)

        # svr_lin = SVR(kernel='linear', C=100, gamma='auto')
        clf = svm.LinearSVR()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.linSVM = ((yTe - np.ravel(x1Te)) ** 2).sum() / x1Te.shape[0]
        print('linSVM prediction:', ((yTr - np.ravel(x1Tr)) ** 2).sum() / x1Tr.shape[0], testError.linSVM)

        clf = svm.SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        print('x1Te.shape[0] = ', x1Te.shape[0])
        testError.rbfSVM = ((yTe - np.ravel(x1Te)) ** 2).sum() / x1Te.shape[0]
        print('rbfSVM prediction:', ((yTr - np.ravel(x1Tr)) ** 2).sum() / x1Tr.shape[0], testError.rbfSVM)

        # svr_poly = SVR(kernel='poly', C=100, gamma='auto', degree=3, epsilon=.1, coef0=1)

    elif flag == 1:

        clf = svm.SVR(gamma='scale')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.SVM = ((yTe - np.ravel(x1Te)) ** 2).sum() / x1Te.shape[0]
        logging.info('SVM prediction: {0:f} {1:f}'.format(((yTr - np.ravel(x1Tr)) ** 2).sum() / x1Tr.shape[0], testError.SVM))

        clf = svm.LinearSVR()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.linSVM = ((yTe - np.ravel(x1Te)) ** 2).sum() / x1Te.shape[0]
        logging.info('linSVM prediction: {0:f} {1:f}'.format(((yTr - np.ravel(x1Tr)) ** 2).sum() / x1Tr.shape[0], testError.linSVM))

        clf = svm.SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.rbfSVM = ((yTe - np.ravel(x1Te)) ** 2).sum() / x1Te.shape[0]
        logging.info('rbfSVM prediction: {0:f} {1:f}'.format(((yTr - np.ravel(x1Tr)) ** 2).sum() / x1Tr.shape[0], testError.rbfSVM))

    return testError

def classTrainTest(testError, f, x0Tr, x1Tr, x0Te, x1Te, flag):

    if flag == 0:

        clf = svm.SVC(class_weight='balanced', gamma='scale')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.SVM = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('SVM prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr))*np.ravel(f.wTr))/f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te))*np.ravel(f.wTe))/f.swTe)

        clf = svm.LinearSVC(class_weight='balanced')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.linSVM = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('linSVM prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr))*np.ravel(f.wTr))/f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te))*np.ravel(f.wTe))/f.swTe)

        clf = RandomForestClassifier(n_estimators=100, class_weight='balanced')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.RF = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('RF prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr))*np.ravel(f.wTr))/f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te))*np.ravel(f.wTe))/f.swTe)

        clf = KNeighborsClassifier()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.knn = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('kNN prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe)

        clf = MLPClassifier(max_iter=10000,hidden_layer_sizes=(100,)*1)
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.mlp = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('MLP prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe)

        clf = GaussianNB()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.GnB = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        print('GnB prediction:', np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr,
              np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe)


    elif flag == 1:

        clf = svm.SVC(class_weight='balanced', gamma='scale')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.SVM = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('SVM prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr, \
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

        clf = svm.LinearSVC(class_weight='balanced')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.linSVM = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('linSVM prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr,
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

        clf = RandomForestClassifier(n_estimators=20, class_weight='balanced')
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.RF = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('RF prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr, \
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

        clf = KNeighborsClassifier()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.knn = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('kNN prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr, \
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

        clf = MLPClassifier(max_iter=10000)
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.mlp = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('MLP prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr, \
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

        clf = GaussianNB()
        clf.fit(x0Tr, np.ravel(x1Tr))
        yTr = clf.predict(x0Tr)
        yTe = clf.predict(x0Te)
        testError.GnB = np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe
        logging.info('GnB prediction: {0:f} {1:f}'.format(
            np.sum(np.not_equal(yTr, np.ravel(x1Tr)) * np.ravel(f.wTr)) / f.swTr, \
            np.sum(np.not_equal(yTe, np.ravel(x1Te)) * np.ravel(f.wTe)) / f.swTe))

    return testError
