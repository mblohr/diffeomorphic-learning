import numpy as np
import numpy.linalg as LA
from base import pointEvolution_fort as pfor
from base import affineBasis


def landmarkEPDiff(T, x0, a0, KparDiff, affine = None, withJacobian=False, withNormals=None, withPointSet=None):
    N = x0.shape[0]
    dim = x0.shape[1]
    timeStep = 1.0/T
    at = np.zeros([T, N, dim])
    xt = np.zeros([T+1, N, dim])
    xt[0, :, :] = x0
    at[0, :, :] = a0
    simpleOutput = True
    if not (withNormals is None):
        simpleOutput = False
        nt = np.zeros([T+1, N, dim])
        nt[0, :, :] = withNormals
    if withJacobian:
        simpleOutput = False
        Jt = np.zeros([T+1, N])
    if not(affine is None):
        A = affine[0]
        b = affine[1]
    if not (withPointSet is None):
        simpleOutput = False
        K = withPointSet.shape[0]
        yt = np.zeros([T+1,K,dim])
        yt[0, :, :] = withPointSet

    for k in range(T):
        z = np.squeeze(xt[k, :, :])
        a = np.squeeze(at[k, :, :])
        xt[k+1, :, :] = z + timeStep * KparDiff.applyK(z, a)
        #print 'test', px.sum()
        if k < (T-1):
            at[k+1, :, :] = a - timeStep * KparDiff.applyDiffKT(z, a[np.newaxis,...], a[np.newaxis,...])
        if not (affine is None):
            xt[k+1, :, :] += timeStep * (np.dot(z, A[k].T) + b[k])
        if not (withPointSet is None):
            zy = np.squeeze(yt[k, :, :])
            yt[k+1, :, :] = zy + timeStep * KparDiff.applyK(z, a, firstVar=zy)
            if not (affine is None):
                yt[k+1, :, :] += timeStep * (np.dot(zy, A[k].T) + b[k])

        if not (withNormals is None):
            zn = np.squeeze(nt[k, :, :])        
            nt[k+1, :, :] = zn - timeStep * KparDiff.applyDiffKT(z, zn[np.newaxis,...], a[np.newaxis,...]) 
            if not (affine is None):
                nt[k+1, :, :] += timeStep * np.dot(zn, A[k])
        if withJacobian:
            Jt[k+1, :] = Jt[k, :] + timeStep * KparDiff.applyDivergence(z, a)
            if not (affine is None):
                Jt[k+1, :] += timeStep * (np.trace(A[k]))
    if simpleOutput:
        return xt, at
    else:
        output = [xt, at]
        if not (withPointSet is None):
            output.append(yt)
        if not (withNormals is None):
            output.append(nt)
        if withJacobian:
            output.append(Jt)
        return output



# Solves dx/dt = K(x,x) a(t) + A(t) x + b(t) with x(0) = x0
# affine: affine component [A, b] or None
# if withJacobian =True: return Jacobian determinant
# if withNormal = nu0, returns nu(t) evolving as dnu/dt = -Dv^{T} nu
def landmarkDirectEvolutionEuler_py(x0, at, KparDiff, affine = None, withJacobian=False, withNormals=None, withPointSet=None):
    N = x0.shape[0]
    dim = x0.shape[-1]
    M = at.shape[0] + 1
    timeStep = 1.0/(M-1)
    xt = np.zeros([M, N, dim])
    xt[0, ...] = x0
    simpleOutput = True
    if not (withNormals is None):
        simpleOutput = False
        nt = np.zeros([M, N, dim])
        nt[0, ...] = withNormals
    if not(affine is None):
        A = affine[0]
        b = affine[1]
    if not (withPointSet is None):
        simpleOutput = False
        K = withPointSet.shape[0]
        yt = np.zeros([M,K,dim])
        yt[0,...] = withPointSet
        if withJacobian:
            simpleOutput = False
            Jt = np.zeros([M, K])
    else:
        if withJacobian:
            simpleOutput = False
            Jt = np.zeros([M, N])

    for k in range(M-1):
        z = np.squeeze(xt[k, ...])
        a = np.squeeze(at[k, ...])
        if not(affine is None):
            Rk = affineBasis.getExponential(timeStep * A[k])
            xt[k+1, ...] = np.dot(z, Rk.T) + timeStep * (KparDiff.applyK(z, a) + b[k])
        else:
            xt[k+1, ...] = z + timeStep * KparDiff.applyK(z, a)
        # if not (affine is None):
        #     xt[k+1, :, :] += timeStep * (np.dot(z, A[k].T) + b[k])
        if not (withPointSet is None):
            zy = np.squeeze(yt[k, :, :])
            if not(affine is None):
                yt[k+1, ...] = np.dot(zy, Rk.T) + timeStep * (KparDiff.applyK(z, a, firstVar=zy) + b[k])
            else:
                yt[k+1, :, :] = zy + timeStep * KparDiff.applyK(z, a, firstVar=zy)
            # if not (affine is None):
            #     yt[k+1, :, :] += timeStep * (np.dot(zy, A[k].T) + b[k])
            if withJacobian:
                Jt[k+1, :] = Jt[k, :] + timeStep * KparDiff.applyDivergence(z, a, firstVar=zy)
                if not (affine is None):
                    Jt[k+1, :] += timeStep * (np.trace(A[k]))
        else:
            if withJacobian:
                Jt[k+1, :] = Jt[k, :] + timeStep * KparDiff.applyDivergence(z, a)
                if not (affine is None):
                    Jt[k+1, :] += timeStep * (np.trace(A[k]))

        if not (withNormals is None):
            zn = np.squeeze(nt[k, :, :])        
            nt[k+1, :, :] = zn - timeStep * KparDiff.applyDiffKT(z, zn[np.newaxis,...], a[np.newaxis,...]) 
            if not (affine is None):
                nt[k+1, :, :] -= timeStep * np.dot(zn, A[k])
    if simpleOutput:
        return xt
    else:
        output = [xt]
        if not (withPointSet is None):
            output.append(yt)
        if not (withNormals is None):
            output.append(nt)
        if withJacobian:
            output.append(Jt)
        return output


def landmarkDirectEvolutionEuler(x0, at, KparDiff, affine = None, withJacobian=False, withNormals=None, withPointSet=None):
    N = x0.shape[0]
    dim = x0.shape[1]
    M = at.shape[0] 
    timeStep = 1.0/(M)
#    xt = np.zeros([M, N, dim])
    simpleOutput = True
    withJ = 0
    withnu = 0
    if not (withNormals is None):
        withnu = 1
        simpleOutput = False
        nt0 = withNormals
    else:
        nt0 = np.zeros([N,dim])
        
    if not(affine is None or len(affine[0])==0):
        A0 = affine[0]
        b = affine[1]
        A = np.zeros([M,dim,dim])
        for k in range(A0.shape[0]):
            A[k,...] = affineBasis.getExponential(timeStep * A0[k])
    else:
        A = np.zeros([M,dim,dim])
        b = np.zeros([M,dim])
        for k in range(M):
            A[k,...] = np.eye(dim) 
        
    if not (withPointSet is None):
        simpleOutput = False
        K = withPointSet.shape[0]
        y0 = withPointSet
        if withJacobian:
            withJ = 1
            simpleOutput = False
    else:
        K = 1
        y0 = np.zeros([K,dim])
        if withJacobian:
            simpleOutput = False

    if KparDiff.localMaps:
        foo = pfor.shoot1orderlocal(x0, at, y0, nt0, A, b, KparDiff.sigma, KparDiff.order,  1+KparDiff.localMaps[0],
                                    1+KparDiff.localMaps[1], withJ, withnu, KparDiff.sigma.size, M, N, dim, K, KparDiff.localMaps[0].size)
    else:
        foo = pfor.shoot1order(x0, at, y0, nt0, A, b, KparDiff.sigma, KparDiff.order, withJ, withnu, KparDiff.sigma.size,
                               M, N, dim, K)
    if simpleOutput:
        return foo[0]
    else:
        output = [foo[0]]
        if not (withPointSet is None):
            output.append(foo[1])
        if not (withNormals is None):
            output.append(foo[3])
        if withJacobian:
            output.append(foo[2])
        return output



def landmarkHamiltonianCovector(x0, at, px1, KparDiff, regweight, affine = None):
    N = x0.shape[0]
    dim = x0.shape[1]
    M = at.shape[0]
    timeStep = 1.0/M
    xt = landmarkDirectEvolutionEuler(x0, at, KparDiff, affine=affine)
    if not(affine is None):
        A0 = affine[0]
        A = np.zeros([M,dim,dim])
        for k in range(A0.shape[0]):
            A[k,...] = affineBasis.getExponential(timeStep * A0[k])
    else:
        A = np.zeros([M,dim,dim])
        for k in range(M):
            A[k,...] = np.eye(dim) 
            
    if KparDiff.localMaps:
        pxt = pfor.adjoint1orderlocal(xt, at, px1, A, KparDiff.sigma, KparDiff.order,  1+KparDiff.localMaps[0],
                                      1 + KparDiff.localMaps[1], regweight, KparDiff.sigma.size,  M, N, dim, KparDiff.localMaps[0].size)
    else:
        pxt = pfor.adjoint1order(xt, at, px1, A, KparDiff.sigma, KparDiff.order, regweight, KparDiff.sigma.size, M, N, dim)
    return pxt, xt

def landmarkHamiltonianCovector_py(x0, at, px1, KparDiff, regweight, affine = None):
    N = x0.shape[0]
    dim = x0.shape[1]
    M = at.shape[0]
    timeStep = 1.0/M
    xt = landmarkDirectEvolutionEuler(x0, at, KparDiff, affine=affine)
    
    pxt = np.zeros([M+1, N, dim])
    pxt[M, :, :] = px1
    if not(affine is None):
        A = affine[0]

    for t in range(M):
        px = np.squeeze(pxt[M-t, :, :])
        z = np.squeeze(xt[M-t-1, :, :])
        a = np.squeeze(at[M-t-1, :, :])
        # dgzz = kfun.kernelMatrix(KparDiff, z, diff=True)
        # if (isfield(KparDiff, 'zs') && size(z, 2) == 3)
        #     z(:,3) = z(:,3) / KparDiff.zs ;
        # end
        a1 = np.concatenate((px[np.newaxis,...], a[np.newaxis,...], -2*regweight*a[np.newaxis,...]))
        a2 = np.concatenate((a[np.newaxis,...], px[np.newaxis,...], a[np.newaxis,...]))
        #a1 = [px, a, -2*regweight*a]
        #a2 = [a, px, a]
        #print 'test', px.sum()
        zpx = KparDiff.applyDiffKT(z, a1, a2)
        #print 'zpx', np.fabs(zpx).sum(), np.fabs(px).sum(), z.sum()
        #print 'pxt', np.fabs((pxt)[M-t-2]).sum()
        if not (affine is None):
            pxt[M-t-1, :, :] = np.dot(px, affineBasis.getExponential(timeStep * A[M - t - 1])) + timeStep * zpx
        else:
            pxt[M-t-1, :, :] = px + timeStep * zpx
    return pxt, xt


# Computes gradient after covariant evolution for deformation cost a^TK(x,x) a
def landmarkHamiltonianGradient(x0, at, px1, KparDiff, regweight, getCovector = False, affine = None):
    (pxt, xt) = landmarkHamiltonianCovector(x0, at, px1, KparDiff, regweight, affine=affine)
    dat = np.zeros(at.shape)
    timeStep = 1.0/at.shape[0]
    if not (affine is None):
        A = affine[0]
        dA = np.zeros(affine[0].shape)
        db = np.zeros(affine[1].shape)
    for k in range(at.shape[0]):
        a = np.squeeze(at[k, :, :])
        px = np.squeeze(pxt[k+1, :, :])
        #print 'testgr', (2*a-px).sum()
        dat[k, :, :] = (2*regweight*a-px)
        if not (affine is None):
            dA[k] = affineBasis.gradExponential(A[k] * timeStep, pxt[k + 1], xt[k]) #.reshape([self.dim**2, 1])/timeStep
            db[k] = pxt[k+1].sum(axis=0) #.reshape([self.dim,1]) 

    if affine is None:
        if getCovector == False:
            return dat, xt
        else:
            return dat, xt, pxt
    else:
        if getCovector == False:
            return dat, dA, db, xt
        else:
            return dat, dA, db, xt, pxt


def secondOrderEvolution(x0, a0, rhot, KparDiff, timeStep, withJacobian=False, withPointSet=None, affine=None):
    T = rhot.shape[0]
    N = x0.shape[0]
    #print M, N
    dim = x0.shape[1]
    at = np.zeros([T+1, N, dim])
    xt = np.zeros([T+1, N, dim])
    xt[0, :, :] = x0
    at[0, :, :] = a0
    simpleOutput = True
    if not(affine is None):
        aff_ = True
        A = affine[0]
        b = affine[1]
    else:
        aff_=False
    if not (withPointSet is None):
        simpleOutput = False
        K = withPointSet.shape[0]
        zt = np.zeros([T+1,K,dim])
        zt[0, :, :] = withPointSet
        if withJacobian:
            simpleOutput = False
            Jt = np.zeros([T+1, K])
    elif withJacobian:
        simpleOutput = False
        Jt = np.zeros([T+1, N])

    for k in range(T):
        x = np.squeeze(xt[k, :, :])
        a = np.squeeze(at[k, :, :])
        #print 'evolution v:', np.sqrt((v**2).sum(axis=1)).sum()/v.shape[0]
        rho = np.squeeze(rhot[k,:,:])
        zx = KparDiff.applyK(x, a)
        za = -KparDiff.applyDiffKT(x, a[np.newaxis,...], a[np.newaxis,...]) + rho
        if aff_:
            #U = np.eye(dim) + timeStep * A[k]
            U = affineBasis.getExponential(timeStep * A[k])
            xt[k+1, :, :] = np.dot(x + timeStep * zx, U.T) + timeStep * b[k]
            Ui = LA.inv(U)
            at[k+1, :, :] = np.dot(a + timeStep * za, Ui)
        else:
            xt[k+1, :, :] = x + timeStep * zx  
            at[k+1, :, :] = a + timeStep * za
        if not (withPointSet is None):
            z = np.squeeze(zt[k, :, :])
            zx = KparDiff.applyK(x, a, firstVar=z)
            if aff_:
                zt[k+1, :, :] =  np.dot(z + timeStep * zx, U.T) + timeStep * b[k]
            else:
                zt[k+1, :, :] = z + timeStep * zx  
            if withJacobian:
                Jt[k+1, :] = Jt[k, :] + timeStep * KparDiff.applyDivergence(x, a, firstVar=z)
                if aff_:
                    Jt[k+1, :] += timeStep * (np.trace(A[k]))
        elif withJacobian:
            Jt[k+1, :] = Jt[k, :] + timeStep * KparDiff.applyDivergence(z, a)
            if aff_:
                Jt[k+1, :] += timeStep * (np.trace(A[k]))
    if simpleOutput:
        return xt, at
    else:
        output = [xt, at]
        if not (withPointSet is None):
            output.append(zt)
        if withJacobian:
            output.append(Jt)
        return output



def secondOrderHamiltonian(x, a, rho, px, pa, KparDiff, affine=None):
    Ht = (px * KparDiff.applyK(x, a)).sum() 
    Ht += (pa*(-KparDiff.applyDiffKT(x, a[np.newaxis,...], a[np.newaxis,...]) + rho)).sum()
    Ht -= (rho**2).sum()/2
    if not(affine is None):
        A = affine[0]
        b = affine[1]
        Ht += (px * (np.dot(x, A.T) + b)).sum() - (pa * np.dot(a, A)).sum()
    return Ht

    
def secondOrderCovector(x0, a0, rhot, px1, pa1, KparDiff, timeStep, affine = None, isjump = None):
    T = rhot.shape[0]
    nTarg = len(px1)
    if not(affine is None):
        aff_ = True
        A = affine[0]
    else:
        aff_ = False
        
    if isjump is None:
        isjump = np.zeros(T, dtype=bool)
        for t in range(1,T):
            if t%nTarg == 0:
                isjump[t] = True

    N = x0.shape[0]
    dim = x0.shape[1]
    [xt, at] = secondOrderEvolution(x0, a0, rhot, KparDiff, timeStep, affine=affine)
    pxt = np.zeros([T+1, N, dim])
    pxt[T, :, :] = px1[nTarg-1]
    pat = np.zeros([T+1, N, dim])
    pat[T, :, :] = pa1[nTarg-1]
    jIndex = nTarg - 2
    for t in range(T):
        px = np.squeeze(pxt[T-t, :, :])
        pa = np.squeeze(pat[T-t, :, :])
        x = np.squeeze(xt[T-t-1, :, :])
        a = np.squeeze(at[T-t-1, :, :])
        #rho = np.squeeze(rhot[T-t-1, :, :])
        
        if aff_:
            U = affineBasis.getExponential(timeStep * A[T - t - 1])
            px_ = np.dot(px, U)
            Ui = LA.inv(U)
            pa_ = np.dot(pa,Ui.T)
        else:
            px_ = px
            pa_ = pa

        #zpx = KparDiff.applyDiffKT(x, [px], [a])

        #zpa = KparDiff.applyK(x, px)
        #print 'zpa1', zpa.sum()
        #zpy = KparDiff.applyDiffKT(x, [a], [px])

        a1 = np.concatenate((px_[np.newaxis,...], a[np.newaxis,...]))
        a2 = np.concatenate((a[np.newaxis,...], px_[np.newaxis,...]))
        zpx = KparDiff.applyDiffKT(x, a1, a2) - KparDiff.applyDDiffK11and12(x, a, a, pa_)
        zpa = KparDiff.applyK(x, px_) - KparDiff.applyDiffK1and2(x, pa_, a)
#               - KparDiff.applyDiffK(x, pa, a) - KparDiff.applyDiffK2(x, pa, a))

        # if aff_:
        #     zpx += np.dot(px, A[T-t-1])
        #     zpa -= np.dot(pa, A[T-t-1].T)

        ### Checking derivatives
        # if affine is None:
        #     afn = None
        # else:
        #     afn = (A[T-t-1], b[T-t-1])
        # H0 = secondOrderHamiltonian(x,a,rho,px,pa,KparDiff, affine=afn)
        # eps = 0.000001
        # dx = eps*np.random.normal(size=x.shape)
        # dH = secondOrderHamiltonian(x+dx,a,rho,px,pa,KparDiff, affine=afn)
        # print 'Test H; x:', (dH - H0)/eps, (zpx*dx).sum()/eps
        # da = eps*np.random.normal(size=a.shape)
        # dH = secondOrderHamiltonian(x,a+da,rho,px,pa,KparDiff, affine=afn)
        # print 'Test H; a:', (dH - H0)/eps, (zpa*da).sum()/eps


        
        #print 'zpa2', zpx.sum()
        pxt[T-t-1, :, :] = px_ + timeStep * zpx
        pat[T-t-1, :, :] = pa_ + timeStep * zpa
        if isjump[T-t-1]:
#            print T-t-1, (T-t-1)/Tsize1
            pxt[T-t-1, :, :] += px1[jIndex]
            pat[T-t-1, :, :] += pa1[jIndex]
            jIndex -= 1

    return pxt, pat, xt, at

# Computes gradient after covariant evolution for deformation cost a^TK(x,x) a
def secondOrderGradient(x0, a0, rhot, px1, pa1, KparDiff, timeStep, isjump = None, getCovector = False, affine=None, controlWeight=1.0):
    (pxt, pat, xt, at) = secondOrderCovector(x0, a0, rhot, px1, pa1, KparDiff, timeStep, isjump=isjump,affine=affine)
    if not (affine is None):
        dA = np.zeros(affine[0].shape)
        db = np.zeros(affine[1].shape)
    Tsize = rhot.shape[0]
    timeStep = 1.0/Tsize
    drhot = np.zeros(rhot.shape)
    if not (affine is None):
        for k in range(Tsize):
            x = np.squeeze(xt[k, :, :])
            a = np.squeeze(at[k, :, :])
            rho = np.squeeze(rhot[k, :, :])
            px = np.squeeze(pxt[k+1, :, :])
            pa = np.squeeze(pat[k+1, :, :])
            zx = x + timeStep * KparDiff.applyK(x, a)
            za = a + timeStep * (-KparDiff.applyDiffKT(x, a[np.newaxis,...], a[np.newaxis,...]) + rho)
            U = affineBasis.getExponential(timeStep * affine[0][k])
            #U = np.eye(dim) + timeStep * affine[0][k]
            Ui = LA.inv(U)
            pa = np.dot(pa, Ui.T)
            za = np.dot(za, Ui)
#            dA[k,...] =  ((px[:,:,np.newaxis]*zx[:,np.newaxis,:]).sum(axis=0)
#                            - (za[:,:,np.newaxis]*pa[:,np.newaxis,:]).sum(axis=0))
            dA[k,...] =  (affineBasis.gradExponential(timeStep * affine[0][k], px, zx)
                          - affineBasis.gradExponential(timeStep * affine[0][k], za, pa))
            drhot[k,...] = rho*controlWeight - pa
        db = pxt[1:Tsize+1,...].sum(axis=1)
        # for k in range(rhot.shape[0]):
        #     #np.dot(pxt[k+1].T, xt[k]) - np.dot(at[k].T, pat[k+1])
        #     #dA[k] = -np.dot(pat[k+1].T, at[k]) + np.dot(xt[k].T, pxt[k+1])
        #     db[k] = pxt[k+1].sum(axis=0)

    #drhot = rhot*controlWeight - pat[1:pat.shape[0],...]
    da0 = KparDiff.applyK(x0, a0) - pat[0,...]

    if affine is None:
        if getCovector == False:
            return da0, drhot, xt, at
        else:
            return da0, drhot, xt, at, pxt, pat
    else:
        if getCovector == False:
            return da0, drhot, dA, db, xt, at
        else:
            return da0, drhot, dA, db, xt, at, pxt, pat

        

